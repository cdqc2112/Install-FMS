#! /bin/bash
#set -euxo pipefail
if [ "$UID" != 0 ]; then
    echo "Run as root"
    exit 1
fi

clear

WORKINGDIR=${PWD}
LOGFILE=$WORKINGDIR/install.log
FMSVER=$(awk -F'=' '/- VERSION=/ {print$2}' $WORKINGDIR/deployment/docker-compose.yml)

export LOGFILE
export WORKINGDIR

touch $LOGFILE
source /etc/os-release

if [ "$ID_LIKE" = "debian" ]; then
    FMS_INSTALLER=apt
else
    FMS_INSTALLER=yum
fi
export FMS_INSTALLER

vi $WORKINGDIR/setup.cfg
source $WORKINGDIR/setup.cfg

echo "Script to install Docker and setup FMS on Ubuntu or CentOS"
echo
# Installation options
if [ "$INSTALL_OPTION" = "offline" ];then
    echo "Offline installation"
elif [ "$INSTALL_OPTION" = "online" ];then 
    echo "Online installation"
fi

# Single or multi node
echo "Storage volume"
echo "The storage can be a file system hosted on a local device, or on network file system (NFS)."
echo "When using a storage on a local device, the backup functionality can be installed, if volumes are created over LVM."
echo
if [ "$NODE" = "singlenode" ];then
    echo "Single node"
elif [ "$NODE" = "multinode" ];then 
    echo "Multi node"
fi

# URL
# if [ -f "$WORKINGDIR/._url" ];then
#     echo "URL $DOMAIN will be used"
# else
#     touch $WORKINGDIR/$DOMAIN.dom
#     touch $WORKINGDIR/._url
# fi

# Certificate
if [ -f "$WORKINGDIR/._certs" ];then
    echo "Certificates created"
else
    read -r -p 'Do you have the certificate? [y/N] ' response
    if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then
            echo "Copy certificate ${DOMAIN}.cert and private key ${DOMAIN}.key to folder $WORKINGDIR"
            echo
            read -n 1 -r -s -p $'Press enter to continue...\n'
            touch $WORKINGDIR/._certs
    else
        read -r -p 'Do you want to generate a private key and a certificate request? [y/N] ' response
        if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then
            ./csr.sh
        else
            read -r -p 'Do you want to generate a self-signed certificate? [y/N] ' response
            if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then
                #./selfSignedCert.sh
                if [ "$REDHAT_SUPPORT_PRODUCT" = "Rocky Linux" ];then
                    cp /etc/pki/tls/openssl.cnf /etc/ssl
                fi
                touch $WORKINGDIR/._self_cert
            fi
        fi
    fi
fi

# Offline Installation
if [ "$INSTALL_OPTION" = "offline" ];then
    while true; do
        if [ ! -f "$WORKINGDIR/images_$FMSVER.tgz" ];
        then
            echo
            read -n 1 -r -s -p $'Required images_$FMSVER.tgz file is missing. Copy the file in $WORKINGDIR and press enter to continue...\n'
            echo
            continue
        fi
    break
done
fi
# Install packages
if [ -f "$WORKINGDIR/._packages" ];then
    echo "Packages installed"
else
    # Offline
    if [ "$INSTALL_OPTION" = "offline" ];then
        cd $WORKINGDIR/packages
        if [ "$FMS_INSTALLER" = "apt" ]; then
            dpkg -i *.deb
        else
            rpm -iUvh *.rpm
        cd $WORKINGDIR
        fi
    echo "$(date): Packages installed" >> $LOGFILE
    touch $WORKINGDIR/._packages
    else
    # Online
        $FMS_INSTALLER install -y \
                dos2unix \
                bash-completion \
                rsync \
                openssl \
                lvm2
    echo "$(date): Packages installed" >> $LOGFILE
    touch $WORKINGDIR/._packages
    fi
fi
# LVM for single node
if [ -f "$WORKINGDIR/._volume" ] || [ -f "$WORKINGDIR/._nfs" ];then
    echo "Volume done"
else 
    if [ "$NODE" = "singlenode" ];then
        DISK=/dev/sdb
        clear
        mkdir -p /opt/fms/solution
        mkdir -p /opt/fms/replication/backup/history
        mkdir -p /mnt/snapshot
        ln -s /opt/fms/solution /opt/fms/master
        read -r -p 'Is there a separated volume for solution and backup? [y/N] ' response
            if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then
                echo "This will create a single LVM to hold the FMS application files and backups"
                echo -e "A separated volume is required and will be erased"
                lsblk
                read -rep "Confirm the disk to be used (will be partitioned): " -i ${DISK} DISK
                parted ${DISK} mklabel msdos
                parted -m -s ${DISK} unit mib mkpart primary 1 100%
                sleep 2
                lsblk
                #read -rep "Confirm the newly created partition on the ${DISK} disk: " -i ${DISK}1 DISK1
                pvcreate ${DISK}1
                vgcreate replica_vg ${DISK}1
                lvcreate -l 25%VG -n replica_live replica_vg
                lvcreate -l 50%VG -n replica_backups replica_vg
                mkfs.xfs /dev/replica_vg/replica_live
                mkfs.xfs /dev/replica_vg/replica_backups
                UUID1=`blkid -o export /dev/replica_vg/replica_live | grep UUID | grep -v PARTUUID`
                UUID2=`blkid -o export /dev/replica_vg/replica_backups | grep UUID | grep -v PARTUUID`
                echo ${UUID1} /opt/fms/solution xfs defaults,nofail 0 0  | tee /etc/fstab -a
                echo ${UUID2} /opt/fms/replication/backup xfs defaults,nofail 0 0  | tee /etc/fstab -a
                mount -avt xfs
                lsblk
            fi
        touch $WORKINGDIR/._volume
        echo "$(date): replica_live replica_vg and replica_backups replica_vg created" >> $LOGFILE
       # read -n 1 -r -s -p $'LVM setup done. Press enter to continue...\n'
        echo "$(date): LVM created" >> $LOGFILE
    else
        # Install NFS client
        ./nfs.sh
    fi
fi
# Install Docker
if [ -f "$WORKINGDIR/.docker" ];then
    echo "Docker installed"
else
    cd $WORKINGDIR
    ./docker.sh
    touch $WORKINGDIR/.docker
    echo "$(date): Docker installed" >> $LOGFILE
fi
# Docker swarm init and node labels
if [ -f "$WORKINGDIR/._swarm" ];then
    echo "Docker swarm init and node labels already done"
else
    # Starting swarm
    #docker swarm init
    ip -br -c -4 addr show | grep UP
    
    while true; do
        read -p "Enter the IP address to be used: " IP_ADDRESS
        echo "IP address: $IP_ADDRESS"
        if [ -n "$IP_ADDRESS" ];then
            docker swarm init --advertise-addr=$IP_ADDRESS
            echo "Starting Swarm"
            break
        else
            echo "IP address is null"
        fi
    done
    # Set node labels
    NODEID=$(docker node ls --format "{{.ID}}")
    docker node update --label-add endpoints=true --label-add role=primary $NODEID
    touch $WORKINGDIR/._swarm
    echo "$(date): Node labels added and swarm started" >> $LOGFILE
fi
# Copy files to /opt/fms/solution
if [ -f "$WORKINGDIR/._files" ];then
    echo "Files already copied"
else
dos2unix deployment/docker-compose.yml
    dos2unix deployment/docker-compose-replication.yml
    dos2unix deployment/includes/*.sh    
    # dos2unix deployment/example.env
    dos2unix deployment/swarm.sh
    dos2unix deployment/backup/*.sh
    chmod +x deployment/swarm.sh deployment/backup/*.sh deployment/includes/*.sh
    cp -r deployment/ /opt/fms/solution
    # mkdir -p /opt/fms/solution/cer
    # cp *.cert *.key /opt/fms/solution/cer
    echo "$(date): Installation files copied to /opt/fms/solution" >> $LOGFILE
    # Create and adjust environment variables in .env file
    touch /opt/fms/solution/deployment/.env
    cat > /opt/fms/solution/deployment/.env <<EOF
TOPOLOGY_API_DNS=api.${DOMAIN}
KEYCLOAK_DNS=auth.${DOMAIN}
TOPOLOGY_UI_DNS=${DOMAIN}
DATA_DNS=data.${DOMAIN}
MESSAGE_DNS=message.${DOMAIN}
ALARM_DNS=alarm.${DOMAIN}
ROOT_DNS=${DOMAIN}
IAM_SERVER=auth.${DOMAIN}
RTU_NTP_SERVER=${NTP}
SNMP_IMPLEMENTATION_VERSION=1
SERVER_CERT_SECRET=
SERVER_CERT_KEY_SECRET=
EOF
    # cp /opt/fms/solution/deployment/example.env /opt/fms/solution/deployment/.env
    # sed -i 's/fms.<customer_domain>/'${DOMAIN}'/g' /opt/fms/solution/deployment/.env
    echo "$(date): DNS set in .env file" >> $LOGFILE
    # sed -i 's/SNMP_IMPLEMENTATION_VERSION=0/SNMP_IMPLEMENTATION_VERSION=1/g' /opt/fms/solution/deployment/.env
    # sed -i 's/RTU_NTP_SERVER=pool.ntp.org/'RTU_NTP_SERVER=${NTP}'/g' /opt/fms/solution/deployment/.env
    # Certs
    if [ ! -f "$WORKINGDIR/._self_cert" ];then
        mkdir -p /opt/fms/solution/cer
        cp *.cert *.key /opt/fms/solution/cer
        docker secret create SERVER_CERT_SECRET /opt/fms/solution/cer/${DOMAIN}.cert
        docker secret create SERVER_CERT_KEY_SECRET /opt/fms/solution/cer/${DOMAIN}.key
        sed -i 's/SERVER_CERT_SECRET=/SERVER_CERT_SECRET=SERVER_CERT_SECRET/g' /opt/fms/solution/deployment/.env
        sed -i 's/SERVER_CERT_KEY_SECRET=/SERVER_CERT_KEY_SECRET=SERVER_CERT_KEY_SECRET/g' /opt/fms/solution/deployment/.env
    fi
    touch $WORKINGDIR/._files
fi
# Workers
if [ "$WORKER" = "true" ] | [ "$REPLICA" = "true" ];then
    clear
    echo "Run setup script on all worker/replica nodes and use this token below to join them to this swarm"
    echo
    docker swarm join-token worker
    echo
    read -n 1 -r -s -p $'When this is done, press enter to continue...\n'
    docker node ls -q role=worker
    echo
    read -p 'Enter the node ID of the worker node: ' WNODEID
    echo
    docker node update --label-add role=primary $WNODEID
fi
# Replica
if [ "$REPLICA" = "true" ];then
    echo
    echo 'Replica node must have joined the swarm before proceeding'
    echo
    docker node ls
    echo
    read -p 'Enter the node ID of the replica node: ' RNODEID
    echo
    docker node update --label-add role=replica $RNODEID
    echo
    sed -i 's|MASTER_ROOT_PATH=/opt/fms/solution|MASTER_ROOT_PATH=/opt/fms/master|g' /opt/fms/solution/deployment/.env
    sed -i 's|REPLICATION_ENABLED=false|REPLICATION_ENABLED=true|g' /opt/fms/solution/deployment/.env
else
    sed -i 's|MASTER_ROOT_PATH=/opt/fms/master|MASTER_ROOT_PATH=/opt/fms/solution|g' /opt/fms/solution/deployment/.env
    sed -i 's|REPLICATION_ENABLED=true|REPLICATION_ENABLED=false|g' /opt/fms/solution/deployment/.env
fi
if [ "$GIS_ADDON" = "yes" ];then
    cp $WORKINGDIR/deployment/gis.addon /opt/fms/solution/deployment
    touch $WORKINGDIR/global.json
    cat > $WORKINGDIR/global.json <<EOF
{
"isCentralizedMode": true,
"delayForSaveGraphMl": 500,
"isGisEnabled": true
}
EOF
else
    rm -rf /opt/fms/solution/deployment/gis.addon
fi
exit