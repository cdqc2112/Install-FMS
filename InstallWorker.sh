#! /bin/bash
#set -euxo pipefail
if [ "$UID" != 0 ]; then
    echo "Run as root"
    exit 1
fi

clear

WORKINGDIR=${PWD}
LOGFILE=$WORKINGDIR/install.log
FMSVER=$(awk -F'=' '/- VERSION=/ {print$2}' $WORKINGDIR/deployment/docker-compose.yml)

export LOGFILE
export WORKINGDIR

touch $LOGFILE
source /etc/os-release

if [ "$ID_LIKE" = "debian" ]; then
    FMS_INSTALLER=apt
else
    FMS_INSTALLER=yum
fi
export FMS_INSTALLER

vi $WORKINGDIR/setup.cfg
source $WORKINGDIR/setup.cfg

echo "Script to install Docker and setup LVM on workers and replica server"
echo
# Installation options
if [ "$INSTALL_OPTION" = "offline" ];then
    echo "Offline installation"
elif [ "$INSTALL_OPTION" = "online" ];then 
    echo "Online installation"
fi
read -r -p 'Is this a replica node [y/N] ' response
    if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
         touch $WORKINGDIR/._replica
    fi
# Install NFS client
if [ -f "$WORKINGDIR/._nfs" ]; then
    echo "NFS done"
else
    ./nfs.sh
fi
clear
# Backup and replica volumes
if [ -f "$WORKINGDIR/._replica" ]; then
    if [ -f "$WORKINGDIR/._rep" ]; then
        echo "Solution and backup volume LVM setup done"
    else
        /opt/fms/master/deployment/backup/./setup.sh
        echo "$(date): Backup and replica volume created" >> $LOGFILE
        touch $WORKINGDIR/._rep
    fi
fi
# Offline Installation
if [ "$INSTALL_OPTION" = "offline" ];then
    while true; do
        if [ ! -f "$WORKINGDIR/images_$FMSVER.tgz" ];
        then
            echo
            read -n 1 -r -s -p $'Required images_$FMSVER.tgz file is missing. Copy the file in $WORKINGDIR and press enter to continue...\n'
            echo
            continue
        fi
    break
done
fi
# Install packages
if [ -f "$WORKINGDIR/._packages" ];then
    echo "Packages installed"
else
    # Offline
    if [ "$INSTALL_OPTION" = "offline" ];then
        cd $WORKINGDIR/packages
        if [ "$FMS_INSTALLER" = "apt" ]; then
            dpkg -i *.deb
        else
            rpm -iUvh *.rpm
        cd $WORKINGDIR
        fi
    echo "$(date): Packages installed" >> $LOGFILE
    touch $WORKINGDIR/._packages
    else
    # Online
        $FMS_INSTALLER install -y \
                dos2unix \
                bash-completion \
                rsync \
                openssl \
                lvm2
    echo "$(date): Packages installed" >> $LOGFILE
    touch $WORKINGDIR/._packages
    fi
fi
# Install Docker
if [ -f "$WORKINGDIR/.docker" ];then
    echo "Docker installed"
else
    cd $WORKINGDIR
    ./docker.sh
    touch $WORKINGDIR/.docker
    echo "$(date): Docker installed" >> $LOGFILE
fi
# Docker swarm join
if [ -f "$WORKINGDIR/._swarm" ];then
    read -n 1 -r -s -p $'Docker swarm join already done. Press enter to continue...\n'
else
    echo "Joining swarm"
    echo 'Run this command on manager "docker swarm join-token worker"'
    read -p $'Paste the string from manager to join the swarm: ' JOIN
    $JOIN
    if test -f "$WORKINGDIR/._replica";then 
        echo "Set the label for replica"
        echo 'Run this command on manager "docker node update --label-add role=replica <nodeidOfReplicaServer>"'
    fi
    touch $WORKINGDIR/._swarm
    echo "$(date): Swarm joined" >> $LOGFILE
fi
if [ "$INSTALL_OPTION" = "offline" ];then
    cd $WORKINGDIR/
    tar -xvf images_$FMSVER.tgz
    cd $WORKINGDIR/images/
    for a in *.tar;do docker load -i $a;done
    cd $WORKINGDIR/
    rm -rf images_$FMSVER.tgz
fi
#Backup
if [ -f "$WORKINGDIR/._replica" ];then
    printf '#!/bin/bash\ncd /opt/fms/master/deployment/backup && exec ./backup.sh > /dev/null 2>&1\n' > /etc/cron.daily/fms_backup
    chmod +x /etc/cron.daily/fms_backup
fi
exit