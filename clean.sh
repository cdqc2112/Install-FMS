#! /bin/bash
if [ "$UID" != 0 ]; then
    echo "Run as root"
    exit 1
fi

clear
echo "You are about to remove FMS solution and data"
read -r -p 'Are you sure you want to remove FMS solution and data? [y/N] ' response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then

    WORKINGDIR=${PWD}

    export LOGFILE
    export WORKINGDIR

    docker stack rm fms
    sleep 20
    docker ps
    docker swarm leave --force
    docker system prune -af
    sleep 10

    cd $WORKINGDIR
    rm ._* info.txt install.log
    rm /etc/cron.daily/fms_backup
    rm -rf /opt/fms/solution/*
    rm -rf /opt/fms/replication/backup/*
    umount /opt/fms/solution
    umount /opt/fms/replication/backup
    umount /opt/fms/replication/replicated_data
    rm -rf /opt/fms
    read -p  "Fstab will open. Please remove lines related to /opt/fms. Press enter to continue"
    vi /etc/fstab
    vgremove replica_vg --force
    echo "FMS solution removed"
fi
exit