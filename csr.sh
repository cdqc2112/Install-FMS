#! /bin/bash

# csr - 1.1

# Create csr & Private key
source $WORKINGDIR/setup.cfg
echo "Creation of a private key and a certificate request (csr) to submit to Certificate Authority (CA) to generate the certificate"
echo
read -e -p 'Enter the country name (2 letter code): ' -i "US" COUNTRY
echo
read -e -p 'Enter the state or province: ' -i "State" STATE
read -e -p 'Enter the locality: ' -i "City" CITY
echo
# Create csr conf
set -euxo pipefail
cat > ${DOMAIN}.conf <<EOF
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = ${COUNTRY}
ST = ${STATE}
L = ${CITY}
CN = ${DOMAIN}

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = ${DOMAIN}
DNS.2 = *.${DOMAIN}
EOF

openssl req -newkey rsa:2048 -nodes -keyout ${DOMAIN}.key -out ${DOMAIN}.csr -config ${DOMAIN}.conf

echo "Copy this csr and submit to Certificate Authotity (ex.: Godaddy, Verisign, Let's Encrypt, ...)"
echo
cat ${DOMAIN}.csr
echo
echo "Wait until you get the certificate to continue"
echo
echo "Copy certificate ${DOMAIN}.cert when received from CA to folder $WORKINGDIR"
read -n 1 -r -s -p $'Press enter to continue...\n'
touch $WORKINGDIR/._certs