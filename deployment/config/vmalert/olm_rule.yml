groups:  
- name: olm_metrics_rules
  rules:      
  - alert: olm_critical_alert
    expr: max by(asset_id) (last_over_time(olm_fault_presence[30m])) == 1 
          and max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) >= 4
          and max by(asset_id) (last_over_time(olm_worst_deviation_element_position[30m]))  > 0
    labels:
      severity: critical
      state: new
      probableCause: Communication subsystem failure
      triggeredThreshold : Link loss deviation
      thresholdLevelUnit : dB
      alarmType: Communication      
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(olm_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      observedThreshold: >
         {{ with printf "last_over_time(olm_worst_deviation_loss{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      positionUnit: >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | label "unit"}}
         {{ end }}
      worstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      absoluteWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_absolute_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      maxWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_max_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      minWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_min_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      thresholdLevel: >
         {{ with printf "last_over_time(olm_link_loss_deviation_threshold{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
  - alert: olm_major_alert
    expr:  max by(asset_id) (last_over_time(olm_fault_presence[30m])) == 1 
           and max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) < 4 
           and (max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) > max by(asset_id) (last_over_time(olm_element_loss_deviation_threshold[30m])) 
           and max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) > max by(asset_id) (last_over_time(olm_link_loss_deviation_threshold[30m])))
           and max by(asset_id) (last_over_time(olm_worst_deviation_element_position[30m]))  > 0  
    labels:
      severity: major      
      state: new
      probableCause: Degraded signal
      triggeredThreshold : Link loss deviation
      thresholdLevelUnit : dB 
      alarmType: Communication
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(olm_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      observedThreshold: >
         {{ with printf "last_over_time(olm_worst_deviation_loss{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      positionUnit: >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | label "unit"}}
         {{ end }}
      worstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      absoluteWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_absolute_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      maxWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_max_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      minWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_min_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      thresholdLevel: >
         {{ with printf "last_over_time(olm_link_loss_deviation_threshold{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      
  - alert: olm_minor_alert
    expr:  (max by(asset_id) (last_over_time(olm_element_loss_deviation_threshold[30m])) < max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) 
            and max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) <= max by(asset_id) (last_over_time(olm_link_loss_deviation_threshold[30m])) 
            and max by(asset_id) (last_over_time(olm_fault_presence[30m])) == 1 and max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) < 4
            and max by(asset_id) (last_over_time(olm_worst_deviation_element_position[30m]))  > 0) 
    labels:
      severity: minor      
      state: new
      probableCause: Degraded signal
      triggeredThreshold : Element loss deviation
      thresholdLevelUnit : dB    
      alarmType: Communication
    annotations:
      thresholdLevel : '{{$value}}'
      sourceResultTime: >
         {{ with printf "last_over_time(olm_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      observedThreshold: >
         {{ with printf "last_over_time(olm_worst_deviation_loss{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      positionUnit: >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])"  .Labels.asset_id | query }}
            {{. | first | label "unit"}}
         {{ end }}
      worstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])"  .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      absoluteWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_absolute_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}     
      maxWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_max_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      minWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_min_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}          

# EFB-6759 major rule for fault at 0 or negative and worst deviation loss > 3dB 
  - alert: olm_major_alert
    expr:  max by(asset_id) (last_over_time(olm_fault_presence[30m])) == 1
           and max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) > 3 
           and max by(asset_id) (last_over_time(olm_worst_deviation_element_position[30m]))  <= 0
    labels:
      severity: major      
      state: new
      probableCause: Fiber break or disconnection between optical test head components
      triggeredThreshold : Element loss deviation
      thresholdLevelUnit : dB 
      alarmType: Communication
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(olm_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      observedThreshold: >
         {{ with printf "last_over_time(olm_worst_deviation_loss{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      positionUnit: >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | label "unit"}}
         {{ end }}
      worstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      absoluteWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_absolute_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      maxWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_max_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      minWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_min_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      thresholdLevel: >
         {{ with printf "last_over_time(olm_link_loss_deviation_threshold{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

# EFB-6759 minor rule for fault at 0 or negative and worst deviation loss < 3dB       
  - alert: olm_minor_alert
    expr:  max by(asset_id) (last_over_time(olm_fault_presence[30m])) == 1 
           and max by(asset_id) (last_over_time(olm_worst_deviation_loss[30m])) < 3
           and max by(asset_id) (last_over_time(olm_worst_deviation_element_position[30m]))  <= 0 
    labels:
      severity: minor      
      state: new
      probableCause: Fiber loss degradation between optical test head components
      triggeredThreshold : Element loss deviation
      thresholdLevelUnit : dB    
      alarmType: Communication
    annotations:
      thresholdLevel : '{{$value}}'
      sourceResultTime: >
         {{ with printf "last_over_time(olm_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      observedThreshold: >
         {{ with printf "last_over_time(olm_worst_deviation_loss{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      positionUnit: >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])"  .Labels.asset_id | query }}
            {{. | first | label "unit"}}
         {{ end }}
      worstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_position{asset_id='%s'}[30m])"  .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      absoluteWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_absolute_worst_deviation_element_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}     
      maxWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_max_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}
      minWorstDeviationPosition : >
         {{ with printf "last_over_time(olm_worst_deviation_element_min_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value}}
         {{ end }}          
