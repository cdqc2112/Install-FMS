groups:
- name: otdr_recording_rules
  interval: 3m
  rules:

    - record: otdr_result_time_recorded
      expr: keep_last_value(label_keep(max by (asset_id, rtu_id) (last_over_time(otdr_result_time[30d])), "asset_id", "rtu_id"))

    - record: otdr_measurement_code_recorded
      expr: keep_last_value(label_keep(otdr_measurement_code, "asset_id", "rtu_id"))

- name: otdr_metrics_rules
  rules:      
  - alert: otdr_break_major_alert
    expr: max by(asset_id) (last_over_time(otdr_fault_loss_delta[30m]))  >= 3 
             and max by(asset_id) (last_over_time(otdr_fault_presence[30m])) == 1 
             and max by(asset_id) (last_over_time(otdr_link_length[30m])) > 50 
             and ( max by(asset_id) (last_over_time(otdr_fault_threshold_name[30m])) == 1 
                or max by(asset_id) (last_over_time(otdr_fault_threshold_name[30m])) == 3)
    labels:
      severity: major
      state: new
      probableCause: Communication subsystem failure
      asset_identity: '{{$labels.asset_id}}'
      asset_type: otdr
      triggeredThreshold : Event loss deviation
      thresholdLevelUnit : dB
      thresholdLevel : 3
      alarmType: Fiber Break
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(otdr_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      faultPosition: >
         {{ with printf "last_over_time(otdr_fault_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
      minFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_min{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
      maxFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_max{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
      
      observedThreshold: >
         {{ with printf "last_over_time(otdr_fault_loss_delta{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
      faultThreshold: >
         {{ with printf "last_over_time(otdr_fault_threshold_name{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

  - alert: otdr_degradation_minor_alert
    expr: max by(asset_id)(last_over_time(otdr_fault_loss_delta[30m]))  < 3 
            and max by(asset_id)(last_over_time(otdr_event_loss_delta[30m]))  < 3 
            and max by(asset_id)(last_over_time(otdr_fault_presence[30m]))  == 1 
            and max by(asset_id)(last_over_time(otdr_link_length[30m]))  > 50 
            and max by(asset_id)(last_over_time(otdr_link_loss_delta[30m]))  < 4 
            and (max by(asset_id)(last_over_time(otdr_fault_threshold_name[30m]))  == 1 
                  or max by(asset_id)(last_over_time(otdr_fault_threshold_name[30m]))  == 3 
                  or max by(asset_id)(last_over_time(otdr_fault_threshold_name[30m]))  == 5)
    labels:
      severity: minor
      state: new
      probableCause: Degraded signal
      asset_identity: '{{$labels.asset_id}}'
      triggeredThreshold : Event loss deviation
      thresholdLevelUnit : dB
      thresholdLevel : < 3
      alarmType: Fiber Degradation
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(otdr_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      faultPosition: >
         {{ with printf "last_over_time(otdr_fault_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      minFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_min{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
      maxFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_max{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      observedThreshold: >
         {{ with printf "last_over_time(otdr_fault_loss_delta{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
      faultThreshold: >
         {{ with printf "last_over_time(otdr_fault_threshold_name{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
           
  - alert: otdr_excessive_loss_minor_alert
    expr: max by(asset_id)(last_over_time(otdr_link_loss_delta[30m]))  >= 4 
             and max by(asset_id)(last_over_time(otdr_fault_presence[30m]))  == 1 
             and max by(asset_id)(last_over_time(otdr_link_length[30m]))  > 50 
             and max by(asset_id)(last_over_time(otdr_fault_threshold_name[30m]))  == 5
    labels:
      severity: minor      
      state: new
      probableCause: Degraded signal
      asset_identity: '{{$labels.asset_id}}'
      triggeredThreshold : Link loss deviation
      thresholdLevelUnit : dB
      thresholdLevel : 4
      alarmType: Fiber excessive loss
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(otdr_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      faultPosition: >
         {{ with printf "last_over_time(otdr_fault_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      minFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_min{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
      maxFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_max{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      observedThreshold: '{{$value}}'
  
  - alert: otdr_injection_level
    expr: max by(asset_id)(last_over_time(otdr_fault_threshold_name[30m]))  == 4 
            and max by(asset_id)(last_over_time(otdr_fault_presence[30m]))  == 1 
            and (max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6008 
            and max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6012 
            and max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6014 
            and max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6015 
            and max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6016)
    labels:
      severity: indeterminate
      state: suppressed
      asset_identity: '{{$labels.asset_id}}_Injection_Level'
      alarmType: Injection level
      triggeredThreshold : Injection Level
      probableCause: Threshold crossed
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(otdr_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      faultPosition: >
         {{ with printf "last_over_time(otdr_fault_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      minFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_min{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
      maxFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_max{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      observedThreshold: >
         {{ with printf "last_over_time(otdr_fault_loss_delta{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      thresholdLevel: >
         {{ with printf "last_over_time(otdr_fault_threshold{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

  - alert: otdr_measurement_error
    expr: (
              (max by(asset_id)(last_over_time(otdr_measurement_code_recorded[30m]))  == 6016 
                  or max by(asset_id)(last_over_time(otdr_measurement_code_recorded[30m]))  == 6008 
                  or max by(asset_id)(last_over_time(otdr_measurement_code_recorded[30m]))  == 6014 
                  or max by(asset_id)(last_over_time(otdr_measurement_code_recorded[30m]))  == 6015
                  or (max by(asset_id)(last_over_time(otdr_measurement_code_recorded[30m]))  == 9999
                  and max by(asset_id)(changes(otdr_measurement_code_recorded[7m])) > 0)
               )
               and max by(asset_id)(last_over_time(otdr_fault_threshold_name[30m]))  != 3 
           )
    labels:
      severity: warning
      state: new
      probableCause: Configuration or customization error
      asset_identity: '{{$labels.asset_id}}'
      alarmType: Measurement Error
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(otdr_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      code: '{{$value}}'

  - alert: otdr_high_reflectance
    expr: max by(asset_id)(last_over_time(otdr_fault_threshold_name[30m]))  == 2 
              and max by(asset_id)(last_over_time(otdr_fault_presence[30m]))  == 1 
              and ( max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6008 
                    and max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6012 
                    and max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6014 
                    and max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6015 
                    and max by(asset_id)(last_over_time(otdr_measurement_code[30m]))  != 6016
                  )
    labels:
      severity: indeterminate
      state: suppressed
      asset_identity: '{{$labels.asset_id}}_High_Reflectance'
      alarmType: High reflectance
      triggeredThreshold : Reflectance
      probableCause: Threshold crossed
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(otdr_result_time{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      faultPosition: >
         {{ with printf "last_over_time(otdr_fault_position{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      minFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_min{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
         
      maxFaultPosition: >
         {{ with printf "last_over_time(otdr_fault_position_max{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      observedThreshold: >
         {{ with printf "last_over_time(otdr_fault_loss_delta{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}

      thresholdLevel: >
         {{ with printf "last_over_time(otdr_fault_threshold{asset_id='%s'}[30m])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }}
       
  - alert: absenceofmeasurement_warning_alert
    expr: max by (asset_id,rtu_id) (rate(otdr_result_time_recorded[6h])) == 0 and 
            ( 
            max by (asset_id,rtu_id) (last_over_time(otdr_measurement_code_recorded[30d])) != 6008 and 
            max by (asset_id,rtu_id) (last_over_time(otdr_measurement_code_recorded[30d])) != 6014
            )
    for: 18h
    labels:
      severity: warning
      triggeredThreshold: Absence of Measurement
      thresholdLevel : 24h
      observedThreshold : "> 24h"
      probableCause: Configuration or customization error
      asset_id: '{{$labels.asset_id}}_AbsenceOfMeasurement'
      or_id : '{{$labels.asset_id}}'
      rtu_id : '{{$labels.rtu_id}}'
    annotations:
      sourceResultTime: >
         {{ with printf "last_over_time(otdr_result_time_recorded{asset_id='%s'}[6h])" .Labels.asset_id | query }}
            {{. | first | value |}}      
         {{ end }} 