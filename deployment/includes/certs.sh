#! /bin/echo not-a-standalone-executable
# shellcheck shell=bash
# Dynamic deps
MODULE_MD5SUM_MODULES_SH=e9fb7bd46882b73e0f76bba5d408a7bf
MODULE_MD5SUM_COLORS_SH=c464cd3bd32e231e9d46fe602cfc6fef


DOTENV_SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

source "$DOTENV_SCRIPT_DIR/modules.sh" -- certs.sh
source "$DOTENV_SCRIPT_DIR/colors.sh"

# Store triplets : cert, key, default function
declare -g CERTIFICATE_WITH_DEFAULT_INIT=()

function checkNoCertificateExists() {
    local I

    if [ "$AUTO_INIT_PRIVATE_CERT_KEEP_EXISTING" = true ]; then
        return
    fi

    if [ "$FORCE_AUTO_INIT_PRIVATE_CA" != true ]; then
        I=0
        while [ "$I" -lt "${#CERTIFICATE_WITH_DEFAULT_INIT[@]}" ]; do
            local CERT_VAR_CERT="${CERTIFICATE_WITH_DEFAULT_INIT[$I]}"
            I=$((I+1))
            local CERT_VAR_KEY="${CERTIFICATE_WITH_DEFAULT_INIT[$I]}"
            I=$((I+2))

            if [ "${!CERT_VAR_KEY:-}${!CERT_VAR_CERT:-}" ]; then
                echo "A certificate is already installed under $CERT_VAR_CERT. To avoid RTU disruption, please ${COLORS[red]}restore the CA key and certificate files${COLORS[none]} in $SSL_DIR matching the installed certificate. Alternatively, you can pass the --force-private-ca switch to bypass this warning." >&2
                exit 1
            fi
        done
    fi
}

function ssl_die() {
    cat "$SSL_DEBUG_FILE" >&2
    exit 255
}

function initSsl() {
    local file

    SSL_DIR="$SWARM_SH_SOURCE_DIR/cer"

    [ -d "$SSL_DIR" ] || mkdir --mode=700 "$SSL_DIR"

    SSL_DEBUG_FILE="$SSL_DIR/.output"

    if [ ! -e "$SSL_DIR/openssl.cnf" ]; then
        for file in /etc/ssl/openssl.cnf /etc/pki/tls/openssl.cnf; do
            if [ -e "$file" ]; then
                cat "$file" > "$SSL_DIR/openssl.cnf"
                break
            fi
        done
        if [ ! -e "$SSL_DIR/openssl.cnf" ]; then
            echo "No openssl.cnf file found on system" >&2
            exit 1
        fi

        cat >> "$SSL_DIR/openssl.cnf" <<'EOF'
[ crl_ext ]
authorityKeyIdentifier=keyid:always

EOF
    fi
    
    if [ ! -e "$SSL_DIR/root.cert" ] || [ ! -e "$SSL_DIR/root.key" ]; then
        # Refuse to create a new CA if any cert is already installed
        checkNoCertificateExists
                    
        echo -n " * Initializing self-signed root CA: " >&2
        openssl req -extensions v3_ca -new -newkey "rsa:$TLS_SELFSIGNED_KEYSIZE" -days 10240 \
                    -config "$SSL_DIR/openssl.cnf" \
                    -nodes -x509 \
                    -sha256 -subj "$TLS_SELFSIGNED_CA_SUBJECT" \
                    -keyout "$SSL_DIR/root.key" -out "$SSL_DIR/root.cert"  > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

        echo "done" >&2
    else
        echo "Reusing root ca : $SSL_DIR/root.*" >&2
    fi

    if [ ! -e "$SSL_DIR/intermediate.cert" ] || [ ! -e "$SSL_DIR/intermediate.key" ]; then
        echo -n " * Initializing intermediate CA from self-signed CA: " >&2

        # Create a certificate signing request
        openssl req -new -newkey "rsa:$TLS_SELFSIGNED_KEYSIZE" -keyout "$SSL_DIR/intermediate.key" \
                    -nodes -days 10240 -sha256 \
                    -config "$SSL_DIR/openssl.cnf" \
                    -subj "$TLS_SELFSIGNED_INTERMEDIATE_SUBJECT" \
                    -out "$SSL_DIR/intermediate.csr" > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

        openssl x509 -req -sha256 -days 10240 -in "$SSL_DIR/intermediate.csr" \
                    -CAcreateserial \
                    -extfile "$SSL_DIR/openssl.cnf" \
                    -extensions v3_ca \
                    -CA "$SSL_DIR/root.cert" \
                    -CAkey "$SSL_DIR/root.key" \
                    -out "$SSL_DIR/intermediate.cert"  > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

        echo "done" >&2
    else
        echo "Reusing intermediate ca : $SSL_DIR/intermediate.*" >&2
    fi
}

function initSslServerCerts() {
    if [ "${#CERTIFICATE_WITH_DEFAULT_INIT[@]}" -lt 4 ]; then
        echo -n " * Initializing server certificate from self-signed CA: " >&2
    elif [ "${#CERTIFICATE_WITH_DEFAULT_INIT[@]}" -gt 0 ]; then
        echo -n " * Initializing server certificates from self-signed CA: " >&2
    fi

    local I=0
    while [ "$I" -lt "${#CERTIFICATE_WITH_DEFAULT_INIT[@]}" ]; do
        local CERT_VAR_CERT="${CERTIFICATE_WITH_DEFAULT_INIT[$I]}"
        I=$((I+1))
        local CERT_VAR_KEY="${CERTIFICATE_WITH_DEFAULT_INIT[$I]}"
        I=$((I+1))
        local CERT_DEFAULT="${CERTIFICATE_WITH_DEFAULT_INIT[$I]}"
        I=$((I+1))
    
        if [ "$AUTO_INIT_PRIVATE_CERT_KEEP_EXISTING" = true ]; then
            if [ "${!CERT_VAR_KEY:-}${!CERT_VAR_CERT:-}" ]; then
                echo "Keeping existing certificate from $CERT_VAR_CERT / $CERT_VAR_KEY" >&2
                continue
            fi
        fi

        # Call the "default" function
        local details
        local detailsArray
        details="$("$CERT_DEFAULT")"
        readarray -t detailsArray <<<"$details"
        CN="${detailsArray[0]}"
        SAN="${detailsArray[1]}"
    
        {
            cat "$SSL_DIR/openssl.cnf"
            echo "[v3_req]"
            echo "basicConstraints = CA:FALSE"
            echo "keyUsage = keyEncipherment, dataEncipherment, nonRepudiation, digitalSignature"
            echo "extendedKeyUsage = serverAuth"
            echo "subjectAltName = $SAN"
        } > "$SSL_DIR/single-$CERT_VAR_KEY.cnf"


        openssl req -new -newkey "rsa:$TLS_SELFSIGNED_KEYSIZE" -keyout "$SSL_DIR/server-$CERT_VAR_KEY.key" \
                    -nodes -days 10240 -sha256 \
                    -config "$SSL_DIR/single-$CERT_VAR_KEY.cnf" \
                    -subj "$TLS_SELFSIGNED_SERVER_SUBJECT/CN=$CN" \
                    -out "$SSL_DIR/server-$CERT_VAR_KEY.csr" > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

        openssl x509 -req -sha256 -days 10240 \
                    -in "$SSL_DIR/server-$CERT_VAR_KEY.csr" \
                    -extfile "$SSL_DIR/single-$CERT_VAR_KEY.cnf" \
                    -extensions v3_req \
                    -CAcreateserial \
                    -CA "$SSL_DIR/intermediate.cert" \
                    -CAkey "$SSL_DIR/intermediate.key" \
                    -out "$SSL_DIR/server-$CERT_VAR_KEY.cert" > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

        local chain
        local key
        local secretName

        printf -v secretName "%s-%(%Y%d%m-%H%M%S)T" "PRIVATE-CA-$CERT_VAR_KEY" -1
        secretName="${secretName,,}"
        secretName="${secretName//_/-}"

        chain="$(cat "$SSL_DIR/server-$CERT_VAR_KEY.cert" "$SSL_DIR/intermediate.cert" "$SSL_DIR/root.cert")"
        key="$(cat "$SSL_DIR/server-$CERT_VAR_KEY.key")"

        createSecret "$secretName-cert" "$chain"
        updateDotEnv "$CERT_VAR_CERT" "$secretName-cert"
        createSecret "$secretName-key" "$key"
        updateDotEnv "$CERT_VAR_KEY" "$secretName-key"
    done

    echo "Done" >&2
}


function initSslClientCerts() {
    local clientCN="$1"
    
    # Force the use of /CN=.../ if not already provided
    if [ "${clientCN#/}" = "$clientCN" ]; then
        clientCN="/CN=$clientCN/"
    fi

    if [ "${EST_CLIENT_CER_SECRET:-}${EST_CLIENT_KEY_SECRET:-}" ]; then
        echo "RTU client certificate already exists. Skipping client certificate creation" >&2
        return
    fi

    echo -n " * Initializing RTU client certificate for $clientCN from self-signed CA: " >&2

    if ! which keytool > /dev/null; then
        echo "The keytool command is not available. Please install the JDK package" >&2
        exit 1
    fi

    local trustStoreSecret
    trustStoreSecret="$( (tr -dc A-Za-z0-9 < /dev/urandom 2> /dev/null || true) | head -c 24)"

    # Create a truststore with the CA certificates
    rm -f "$SSL_DIR/keystore-new.jks"
    keytool -importcert -alias fgms-ca \
            -keystore "$SSL_DIR/keystore-new.jks" \
            -storepass "$trustStoreSecret" \
            -file "$SSL_DIR/root.cert" -noprompt > "$SSL_DEBUG_FILE" 2>&1 || ssl_die
    keytool -importcert -alias fgms-intermediate \
            -keystore "$SSL_DIR/keystore-new.jks" \
            -storepass "$trustStoreSecret" \
            -file "$SSL_DIR/intermediate.cert" -noprompt > "$SSL_DEBUG_FILE" 2>&1 || ssl_die
    mv -f "$SSL_DIR/keystore-new.jks" "$SSL_DIR/keystore.jks"

    printf -v secretName "%s-%(%Y%d%m-%H%M%S)T" "rtu-keystore" -1

    createSecret "$secretName-password" "$trustStoreSecret"
    createSecret --from-file "$secretName-keystore" "$SSL_DIR/keystore.jks"

    updateDotEnv "RTU_SSL_AUTH_TRUSTSTORE_PASSWD_SECRET" "$secretName-password"
    updateDotEnv "RTU_SSL_AUTH_TRUSTSTORE_FILE_SECRET" "$secretName-keystore"


    # Each client certificate is exposed as EST:
    # EST_CLIENT_KEY_SECRET=
    # EST_CLIENT_CER_SECRET=
    # EST_CLIENT_CACER_SECRET=
    
    {
        cat "$SSL_DIR/openssl.cnf"
        echo "[v3_req]"
        echo "basicConstraints = CA:FALSE"
        echo "keyUsage = keyEncipherment, dataEncipherment, nonRepudiation, digitalSignature"
        echo "extendedKeyUsage = clientAuth"
    } > "$SSL_DIR/rtuclient.cnf"

    # Create a certificate signing request
    openssl req -newkey "rsa:$TLS_SELFSIGNED_KEYSIZE" -keyout "$SSL_DIR/rtuclient.key" \
                -nodes -days 10240 -sha256 \
                -config "$SSL_DIR/rtuclient.cnf" \
                -subj "$clientCN" \
                -out "$SSL_DIR/rtuclient.csr" > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

    # Sign the certificate
    openssl x509 -req -sha256 -days 10240 \
                -in "$SSL_DIR/rtuclient.csr" \
                -extfile "$SSL_DIR/rtuclient.cnf" \
                -extensions v3_req \
                -CAcreateserial \
                -CA "$SSL_DIR/intermediate.cert" \
                -CAkey "$SSL_DIR/intermediate.key" \
                -out "$SSL_DIR/rtuclient.cert" > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

    # Convert the key to PKCS8 format
    openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt \
                -in "$SSL_DIR/rtuclient.key" \
                -out "$SSL_DIR/rtuclient.key.pkcs8" > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

    # Create a PKCS7 file with the client certificate and the CA chain
    openssl crl2pkcs7 -nocrl -certfile "$SSL_DIR/rtuclient.cert" \
                -out "$SSL_DIR/rtuclient.cert.pkcs7"  > "$SSL_DEBUG_FILE" 2>&1 || ssl_die
    
    # Create a PKCS7 file with the CA chain
    cat "$SSL_DIR/intermediate.cert" "$SSL_DIR/root.cert" > "$SSL_DIR/cacerts.pem"
    openssl crl2pkcs7 -nocrl -certfile "$SSL_DIR/cacerts.pem" \
                -out "$SSL_DIR/cacerts.pem.pkcs7" > "$SSL_DEBUG_FILE" 2>&1 || ssl_die

    local secretName

    printf -v secretName "%s-%(%Y%d%m-%H%M%S)T" "PRIVATE-CA-RTU-CLIENT" -1
    secretName="${secretName,,}"
    secretName="${secretName//_/-}"

    createSecret --from-file "$secretName-cacer" "$SSL_DIR/cacerts.pem.pkcs7"
    updateDotEnv "EST_CLIENT_CACER_SECRET" "$secretName-cacer"
    createSecret --from-file "$secretName-cert" "$SSL_DIR/rtuclient.cert.pkcs7"
    updateDotEnv "EST_CLIENT_CER_SECRET" "$secretName-cert"
    createSecret --from-file "$secretName-key" "$SSL_DIR/rtuclient.key.pkcs8"
    updateDotEnv "EST_CLIENT_KEY_SECRET" "$secretName-key"
    echo "Done" >&2
}

function sslWarn() {
    echo "${COLORS[orange]}A certificate from private CA was created. Make sure to ${COLORS[none]}" >&2
    echo " * ${COLORS[red]}Install the CA${COLORS[none]} certificate file $SSL_DIR/root.cert on the RTUs and user's devices" >&2
    echo " * ${COLORS[red]}Secure/backup the CA directory${COLORS[none]} $SSL_DIR, to allow future renewall of FMS certificate" >&2
}

