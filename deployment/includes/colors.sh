#! /bin/echo not-a-standalone-executable
# shellcheck shell=bash
# Dynamic deps
MODULE_MD5SUM_MODULES_SH=e9fb7bd46882b73e0f76bba5d408a7bf

DOTENV_SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

source "$DOTENV_SCRIPT_DIR/modules.sh" -- colors.sh

# Some colors
# Per value assignment is a workaround for bash bug in older version
# where the -g flag is not well supported
declare -g -A COLORS
COLORS[black]='0;30'
COLORS[red]='0;31'
COLORS[green]='0;32'
COLORS[orange]='0;33'
COLORS[blue]='0;34'
COLORS[purple]='0;35'
COLORS[cyan]='0;36'
COLORS[light-gray]='0;37'
COLORS[dark-gray]='1;30'
COLORS[light-red]='1;31'
COLORS[light-green]='1;32'
COLORS[yellow]='1;33'
COLORS[light-blue]='1;34'
COLORS[light-purple]='1;35'
COLORS[light-cyan]='1;36'
COLORS[white]='1;37'
COLORS[none]='0'


if [ -t 1 ] && tput colors > /dev/null 2>&1; then
    for c in "${!COLORS[@]}"; do
        COLORS[$c]="$(echo -ne "\033[${COLORS[$c]}m" )"
    done
else
    for c in "${!COLORS[@]}"; do
        COLORS[$c]=''
    done
fi
