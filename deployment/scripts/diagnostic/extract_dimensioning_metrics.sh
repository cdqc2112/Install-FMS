#!/bin/bash

set -ueo pipefail

# shellcheck source=/dev/null
builtin source "$(dirname "$0")/utils.sh"

verbose="false"

function output_add() {

	content=$(< /dev/stdin)
	
	OUTPUT=$(jq --null-input "${OUTPUT} + $content")
}

function status_msg() {
	if [ "$verbose" = "true" ] ; then
		if [[ $# = 2  &&  ( "$2" = "-c" || "$2" = "--clear" ) ]]; then
			printf "%s\r" "$1"
		else
			printf "%s\n" "$1" 
		fi
	fi
}

########################
#  MAIN - ENTRY POINT  #
########################

# check dependencies
[ "$BASH_VERSION" ] || { echo "Bash execution required"; showUsage; exit 1; }
jq --version >/dev/null 2>&1 || { echo "jq required"; exit 1 ; }

# Variables
OUTPUT_FILENAME=""
SET_COMPACT_RESULT="false"
SET_OUTPUT_FILE="false"
SET_OUTPUT_FILENAME_DEFINED=""

export OUTPUT="{}"

# HELP / USAGE command
for var in "$@"; do
	if [[ " $var " = " -h " ]] || [[ " $var " =~ " --help " ]] ; then
		showUsage
		exit
	fi

	if [[ " $var " = " -c " ]] ; then
		SET_COMPACT_RESULT="true"
		continue
	fi

	if [[ " $var " = " -f " ]] ; then
		SET_OUTPUT_FILE="true"
		SET_OUTPUT_FILENAME_DEFINED="false"
		continue
	fi

	if [[ " $SET_OUTPUT_FILENAME_DEFINED " = " false " ]] ; then
		OUTPUT_FILENAME=$var
		SET_OUTPUT_FILENAME_DEFINED="true"
		continue
    fi

	if [ "$var" = "-v" ]; then
		verbose="true"
	fi

done

# Check fms_monitor-prometheus is available
PROMETHEUS_EXIST=$(docker service inspect fms_monitor-prometheus | jq '.[].ID' 2>/dev/null)
if [ -z "$PROMETHEUS_EXIST" ]
then
	echo "fms_monitor-prometheus service is not running, please check self-monitoring addon deployment"
	#exit 1
fi


# Test the number of input parameters
if [ $# -gt 5 ]
  then
		showUsage
		exit 1
fi

##############################
#  BEGIN metrics extraction  #
##############################

function extract_metrics() {
	# GET METRICS : FMS version
	status_msg "gathering fms version..."
	output_add <<< "$(get_fms_version)"

	# GET METRICS : FMS OS version and FMS Kernel version
	status_msg "gathering os and kernel version..."
	output_add <<< "$(get_fms_os_and_kernel_versions)"

	# GET METRICS : fms number of users
	status_msg "gathering fms number of users..."
	output_add <<< "$(get_fms_nbusers)"

	# GET METRICS : fms ip route table
	status_msg "gathering ip route table..."
	output_add <<< "$(get_fms_ip_route_tables)"

	# GET METRICS : fms docker
	status_msg "gathering fms docker info..."
	output_add <<< "$(get_docker_info)"

	# GET METRICS : fms docker nodes informations
	status_msg "gathering fms node info..."
	output_add <<< "$(get_node_info)"

	# GET METRICS : API registration
	status_msg "gathering api registration..."
	output_add  <<< "$(get_registration_metric)"

	# GET METRICS : get nfs state info
	status_msg "gathering nfs state info..."
	output_add  <<< "$(check_nfs)"

	# GET METRICS : Topology
	declare -a list_metrics

	# This array contains the Topology metrics to extract.
	# The queries to get the metrics value are defined in "pg_queries_topology.yaml" file.
	# Each line of the array contains 3 fields separated by ";" :
	#	1) the metric to extract
	#	2) the relative query in "pg_queries_topology.yaml"
	#	3) the list of dimensions associated to the metrics (if there is any)

	list_metrics=(
		"numberOfRtus;pg_tp_rtus_nbrtus"
		"numberOfDiagrams;pg_tp_diagrams_nbdiagrams"
		"numberOfDomains;pg_tp_domains_nbdomains"
		"numberOfSites;pg_tp_sites_nbsites"
		"numberOfOpticalRoutes;pg_tp_opticalroutes_nbopticalroutes"
		"numberOfOpticalDevices;pg_tp_optical_devices_nbopticaldevices"
		"avgNumberOfPortsAvailablePerAttachedRtu;pg_ports_available_per_attached_rtu_avg_nb_ports"
		"avgNumberOfPortsUsedPerAttachedRtu;pg_ports_used_per_attached_rtu_avg_nb_ports"
		"numberOfRtusPerModelVersionAttachstatus;pg_rtus_model_status_version_nb_rtus;model|attachstatus|version"
		"numberOfP2pOpticalRoutes;pg_p2p_opticalroutes_nb_routes"
		"numberOfP2pOpticalRoutesAttached;pg_p2p_opticalroutes_attached_nb_routes"
		"numberOfP2pOpticalRoutesUnattached;pg_p2p_opticalroutes_unattached_nb_routes"
		"numberOfPonOpticalRoutes;pg_pon_opticalroutes_nb_routes"
		"numberOfPonOpticalRoutesAttached;pg_pon_opticalroutes_attached_nb_routes"
		"numberOfPonOpticalRoutesUnattached;pg_pon_opticalroutes_unattached_nb_routes"
		"avgNumberOfSegmentsPerP2pOpticalRoutes;pg_cable_segments_p2p_opticalroutes_avg_cable_segments"
		"avgNumberOfSegmentsPerP2pOpticalRoutesAttached;pg_cable_segments_p2p_opticalroutes_attached_avg_cable_segments"
		"avgNumberOfSegmentsPerP2pOpticalRoutesUnattached;pg_cable_segments_p2p_opticalroutes_unattached_avg_cable_segments"
		"avgNumberOfSegmentsPerPonOpticalRoutes;pg_cable_segments_pon_opticalroutes_avg_cable_segments"
		"avgNumberOfSegmentsPerPonOpticalRoutesAttached;pg_cable_segments_pon_opticalroutes_attached_avg_cable_segments"
		"avgNumberOfSegmentsPerPonOpticalRoutesUnattached;pg_cable_segments_pon_opticalroutes_unattached_avg_cable_segments"
		"avgNumberOfObjectTypePerDiagram;pg_diagrams_objects_avg_objecttype_per_diagram;objecttype"
		"avgNumberOfUsersPerDomain;pg_domains_users_avg_nb_users_per_domain"
		"avgNumberOfObjectTypePerDomain;pg_domains_objects_avg_objecttype_per_domain;objecttype"
		"numberOfModulesPerTypeVersionAttachstatus;pg_modules_type_status_version_nb_modules;type|attachstatus|version"
		"avgNumberOfPortsPerOpticalDevice;pg_optical_devices_ports_avg_ports_number_per_type;type"
		)

	# Get metric value for each metric of "list_metrics"
	i=0
	for metric in "${list_metrics[@]}"
	do
		output_add <<< "$(get_topology_metric "${metric}")"

		i=$((i+1))
		status_msg "$(printf "collecting metric value: %s / %s" $i ${#list_metrics[@]})" -c
	done
	status_msg ""

	# GET METRICS : fms sizing data
	status_msg "gathering sizing data..."
	output_add <<< "$(get_sizing_data)"

	status_msg "gathering configuration data..."
	output_add <<< "$(get_conf_data)"

	status_msg "gathering container memory usage..."
	output_add <<< "$(get_container_memory_usage)"

	status_msg "gathering network data..."
	output_add <<< "$(get_network_data)"

	status_msg "checking .env data..."
	output_add <<< "$(check_env)"

	status_msg "checking files..."
	output_add <<< "$(check_files)"

	output_add <<< "$(check_config_overide)"

	status_msg "checking smtp server..."
	output_add <<< "$(check_smtp_server)"

	status_msg "checking snmp server..."
	output_add <<< "$(check_snmp_server)"

	status_msg "checking ca certificate..."
	output_add <<< "$(check_ca_certificate)"
}

status_msg "starting data collecting"

extract_metrics

status_msg "done !"

###########################
# END metrics extraction  #
###########################

# Finalize process

# Define the compact result option
if [ $SET_COMPACT_RESULT = "true" ]
then
	OUTPUT=(jq -c "$OUTPUT")
fi

# Save the results in output file if the option has been set, otherwise display metrics
if [ $SET_OUTPUT_FILE = "true" ]
then
	touch $OUTPUT_FILENAME
	chown "$(whoami)" $OUTPUT_FILENAME
	# Put extracted metrics into ouptut file
	echo "${OUTPUT[@]}" > "$OUTPUT_FILENAME"
else
	# Display extracted metrics in current window
	status_msg "#=========================# DATA #=========================#"
	echo "${OUTPUT[@]}"
fi
