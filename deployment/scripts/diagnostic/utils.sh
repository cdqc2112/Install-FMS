#!/bin/bash -x

CURRENT_SCRIPT=$(basename "$0")

# Set the default field separator
IFS=' '

function to_list () {
	jq -n "[]$(echo -e "$1" | sed 's/\(^.*$\)/+[\"\1\"]/g' | tr -d "\n")"
}

# Define functions
function showUsage() {
	printf '\n'
	printf '%s\n' "Usage:"
	printf '%s\n' "	# For display mode"
	printf '%s\n\n' "	./${CURRENT_SCRIPT}"
	printf '%s\n' "	# For compacting JSON result without line breaks"
	printf '%s\n\n' "	./${CURRENT_SCRIPT} -c"
	printf '%s\n' "	# For JSON output file mode"
	printf '%s\n\n' "	./${CURRENT_SCRIPT} -f <JSON output file name>"
	printf '\n'
	printf '%s\n' "	# Verbose"
	printf '%s\n\n' "	./${CURRENT_SCRIPT} -v"
	printf '\n'
}

function get_fms_version() {
	FMS_VERSION=$(grep -i "\- version=" /opt/fms/solution/deployment/docker-compose.yml|cut -d'=' -f2)
	echo "{\"fmsVersion\": \"${FMS_VERSION}\"}"
}


function get_fms_os_and_kernel_versions() {
	FMS_OS_VERSION=$(hostnamectl | grep -i operating | cut -d":" -f2 | sed "s/^\ *//g")
    FMS_KERNEL_VERSION=$(hostnamectl | grep -i kernel | cut -d":" -f2 | sed "s/^\ *//g")
    
	echo "{\"fmsOsVersion\": \"${FMS_OS_VERSION}\", \"fmsKernelVersion\": \"${FMS_KERNEL_VERSION}\"}"
}


function get_docker_info() {

	# gathering data
	DOCKER_LOG_ROTATIONS=$(cat /etc/docker/daemon.json)
	DOCKER_IMAGES_DIGESTS=$(docker images --digests --format "{ {{json .Repository}} : {{json .Digest}} }" | jq -s "add")
	INTERNET_CONNECTION=$(nslookup www.exfo.com 8.8.8.8 &> /dev/null && echo "connected successfully to www.exfo.com" || echo "no connection to www.exfo.com")
	DOCKER_SERVICE_LIST=$(docker service ls --format "{\"{{.Name}}\":\"{{.Replicas}}\"}" | jq -s "add")
	
	# adding data to json
	echo "{\"dockerImagesDigests\": ${DOCKER_IMAGES_DIGESTS}, \
        \"dockerLogRotation\":${DOCKER_LOG_ROTATIONS}, \
        \"internetConnectionStatus\":\"${INTERNET_CONNECTION}\", \
		\"dockerServices\":${DOCKER_SERVICE_LIST}}"

}

function get_node_info() {

	RES="{}"
	FMS_NODES_DATAS=""
	proxy_container_id=$(docker ps -aqf "name=fms_proxy")

	# get nodes informations
	NODES_IDS=$(docker node ls --format '{{json .ID}}' | tr -d "\"" | tr "\n" " ")

	DISK_USAGE_DATA=$(docker exec "$proxy_container_id" curl -s "http://monitor-prometheus:9090/api/v1/query?query=100%20*%20avg%20by%20(name%2C%20mountpoint)%20((avg%20by%20(instance%2C%20mountpoint)%20(node_filesystem_free_bytes%7Bmountpoint%3D~%22%2F.*%22%7D)%20%2F%20avg%20by%20(instance%2C%20mountpoint)%20(node_filesystem_size_bytes%7Bmountpoint%3D~%22%2F.*%22%7D))%20*%20on(instance)%20group_left%20(name)%20hostname)")
	
	for id in ${NODES_IDS}
	do
		# gathering node data
		NODE_DATA=$(docker node inspect --format docker "${id}" --format '{"ID" : {{json .ID}},"EngineVersion" : {{json .Description.Engine.EngineVersion}}, "Hostname" : {{json .Description.Hostname}}, "Labels": {{json .Spec.Labels}},"Resources" : {{json .Description.Resources}},  "Status" : {{json .Status.State}},"Availability" : {{json .Spec.Availability}},"ManagerStatus" : {{json .ManagerStatus}}}')
		NODE_DATA=$(echo "${NODE_DATA}" | jq "if .ManagerStatus.Leader==true then . else .ManagerStatus += {\"Leader\":false} end")

		# checking netcat connection
		IP=$(jq --null-input -r "${NODE_DATA}.ManagerStatus.Addr" | cut -d":" -f1 )
		PORT=$(jq --null-input -r "${NODE_DATA}.ManagerStatus.Addr" | cut -d":" -f2)
		NC=$(netcat -w1 "$IP" "$PORT" -v 2>&1)
		
		if [[  $(jq -n "$NODE_DATA.Hostname") != *"replica"* ]] && ! echo "$NC" | grep -q "command not found"; then
			NODE_DATA=$(jq --null-input "${NODE_DATA} + {\"NetcatOutput\": \"${NC}\"}")
		fi

		name=$(jq -nr "$NODE_DATA.Hostname" | cut -d"." -f1)
		NODE_DISK_USAGE=$(jq -n "[ $DISK_USAGE_DATA.data.result[] | if .metric.name == \"$name\" then {(.metric.mountpoint) : (.value[1])}  else null end]| add")
		NODE_DATA=$(jq -n "$NODE_DATA + {\"DiskUsage\" : $NODE_DISK_USAGE}")

		FMS_NODES_DATAS="${FMS_NODES_DATAS}${NODE_DATA}"
	done

    RES=$(echo "$FMS_NODES_DATAS" | jq -s)

	echo "{\"nodesDatas\": ${RES}}"
}

function get_fms_nbusers() {
    
    # Get container id for proxy
    proxy_container_id=$(docker ps -aqf "name=fms_proxy")
	
    query_url='http://service-authorization-proxy:8085/users?max=10000'
	cmd=(curl -s -X GET -H "OUTPUT-Type: application/json" -H "x-Token-Roles: [\"fg-topology-master\", \"fg-topology-read\"]]" "$query_url")
	metric_value=$(docker exec "$proxy_container_id" "${cmd[@]}" | jq '.[].id' | wc -l)

	# Add result to json
	echo "{\"fmsNbUsers\": \"${metric_value}\"}"
}

function get_fms_ip_route_tables() {

	RES="{}"

	# Get data
	IPTABLES_RES=$(iptables -L DOCKER-USER)

	# Extract data
	IPTABLES_RES=$(echo "${IPTABLES_RES}" | tr -s "  " "|" | tr "\n" " " |sed -e 's/||/|null|/g' | cut -d "|" -f9-)
	
	# Format data in json
	declare -a data_list
	ip_data=
	for rule in $IPTABLES_RES; do

		# format rule
		rule=$(echo "$rule" | tr "|" " " )
		read -ra data_list <<< "$rule"

		# concatenate cells 6+ into one cell
		data_list[5]=$(echo "${data_list[@]}" | cut -d" " -f6-)
		data_list=("${data_list[@]:0:6}")

		# format rule in json
		tmp=$(printf '{"target": "%s", "prot": "%s", "opt": "%s", "source": "%s", "destination": "%s", "other":"%s"}\n' "${data_list[@]}")

		# add rule to the other
		ip_data="$ip_data$tmp"
	done

	RES=$(echo "$ip_data" | jq -s)

	# Add ip route tables to json
	echo "{\"ipRouteTables\":$RES}"
}

function check_nfs() {

    RES=""

	# Get nfs status
	NFS3=$(mount -t nfs)
	NFS4=$(mount -t nfs4)

	# Format output
	NFS3=$(to_list "$NFS3")

	# Format output
	NFS4=$(to_list "$NFS4")

	# Add result to json
	RES="{\"V3\":${NFS3}, \"V4\":${NFS4}}"

	# Add root squash data if nfs active
	if [[ -z $NFS3 ]] && [[ -z $NFS4 ]]; then
		RES=$(jq -n "$RES + {\"rootSquash\":null}")
        echo "{\"nfs\":$RES}"
		return
	fi

	if [ -f ./tmp_root_test ]; then
		rm -f /opt/fms/solution/tmp_root_test
	fi

	# Check root squash
	sudo touch /opt/fms/solution/tmp_root_test
	ROOT_SQUASH=$(stat -c "%u %g" /opt/fms/solution/tmp_root_test)

	# Add result to json
	RES=$(jq --null-input "$RES+{\"rootSquashTest\":\"${ROOT_SQUASH}\"}")
	
	sudo rm /opt/fms/solution/tmp_root_test

    echo "{\"nfs\":$RES}"
}

function get_topology_metric() {

	# initialize var
	metric="$1"
	metric_display=$(echo "$metric" | cut -d";" -f1)
	metric_name=$(echo "$metric" | cut -d";" -f2)
	metric_columns=$(echo "$metric" | cut -d";" -f3)
	
	# Api call command
	query_url='http://monitor-prometheus:9090/api/v1/query?query='${metric_name}
	cmd=(curl -s -X GET -H "OUTPUT-Type: application/json" "$query_url")
	
    proxy_container_id=$(docker ps -aqf "name=fms_proxy")

	if [ "$metric_columns" = "" ]
	then
		# Execute command on fms_proxy container
		metric_value=$(docker exec "$proxy_container_id" "${cmd[@]}" | jq '.data.result[] | select(.metric.role=="primary") | .value[1]'| sed -e 's/^"NaN"$/null/g' | sed -e 's/\\\"/\"/g')
	else
		# prefix for jq filter
		jq_columns_filter='[.data.result[]|select(.metric.role=="primary")|{'
		
		# add columns to display in jq filter	
		tmp_filter=$(echo "$metric_columns" | awk 'BEGIN {FS="|";outputvar=""} {for(i=1 ; i <= NF ; i++) {outputvar=outputvar $i ":." "metric." $i "," }} {print outputvar}')

		# suffix for the jq filter : get the value of the counter
		jq_columns_filter="${jq_columns_filter}${tmp_filter}value:.value[1]}]"

		# Execute command on fms_proxy container
		metric_value=$(docker exec "$proxy_container_id" "${cmd[@]}" | jq "${jq_columns_filter}" |  sed -e 's/\\\"/\"/g' | sed -e 's/\"{/{/g' | sed -e 's/}\"/}/g' )
	fi

	if [[ -z "$metric_value" ]]; then
		metric_value="\"\""
	fi

	# add the result to json
	echo "{\"${metric_display}\": ${metric_value}}"
}

function get_registration_metric() {

	# Backup the field separator
	old_IFS=$IFS
	
	# New field separator
	IFS=$'\n'
	
	# initializing var
	DIR_LOGS_PROXY=/opt/fms/solution/logs/proxy
	uid=$(stat -c "%u" /opt/fms/solution/logs/proxy/)

	all_calls=0
	calls_with_serialized=0

	FILE_LIST=$(sudo -u "#$uid" ls -t $DIR_LOGS_PROXY | grep access.log)

	for myfile in ${FILE_LIST}
	do
		if [ "${all_calls}" == "0" ]
		then
			# Count numbers of calls to GET /api/topology/control/remotetestunits/registration (total and with serialization only)
			NB_REGISTRATION_API_CALLS=$(sudo -u "#$uid" awk '
			/GET \/api\/topology\/control\/remotetestunits\/registration/ {all_calls++} 
			/GET \/api\/topology\/control\/remotetestunits\/registration?.*showSerialized/ {calls_with_serialized++} 
			END {printf "%d;%d",all_calls,calls_with_serialized}' "$DIR_LOGS_PROXY/${myfile}")

			all_calls=$(echo "$NB_REGISTRATION_API_CALLS" | cut -d";" -f1)
			calls_with_serialized=$(echo "$NB_REGISTRATION_API_CALLS" | cut -d";" -f2)
		fi
	done

	# add result to json
	echo "{\"nbRegistrationApiCalls\": \"${all_calls}\", \
	    \"nbRegistrationApiSerializedCalls\": \"${calls_with_serialized}\"}"

	# Restore the field separator
	IFS=$old_IFS
}

function get_sizing_data () {

	RES="{}"

	# sizing services list

	services=(
		"fms_measurement-data-db"
		"fms_alarming-db"
		"fms_rtu-api-gateway-db"
		"fms_victoria-metrics"
		"fms_vmalert"
	)

	# collect services sizing data
	jsonPath=".Spec.TaskTemplate.Resources.Limits.MemoryBytes"

	for service in "${services[@]}"; do
		MEMORY_LIMIT=$(docker service inspect "$service" --format "{{json $jsonPath}}" | numfmt --to=si)
		
		RES=$(jq --null-input "$RES + {\"$service\":\"$MEMORY_LIMIT\"}")
	done

	# Add result to json
	echo "{\"sizingData\":${RES}}"
}

function get_container_memory_usage () {

	RES="{}"

	# gather data an format
	MEMORY_USAGE=$(docker stats --no-stream --format "{\"{{.Name}}\": [\"{{.MemPerc}}\",\"{{.MemUsage}}\"]}")
	RES=$(echo "$MEMORY_USAGE" |jq -s "add")

	# Add result to json
	echo "{\"containerMemoryUsage\":$RES}"
}

function get_network_data() {
	# Get IP 4/6
	RES="{}"

	dns_name=$(grep ROOT_DNS < .env | cut -d "=" -f2 | tr -d "\"")

    IP4=$(echo Q | openssl s_client -connect "$dns_name:443" 1> /dev/null 2> /dev/null -4 && echo "true" || echo "false")
    IP6=$(echo Q | openssl s_client -connect "$dns_name:443" 1> /dev/null 2> /dev/null -6 && echo "true" || echo "false")

	RES=$(jq --null-input "${RES} + {\"IPv4\":${IP4}, \"IPv6\":${IP6}}")

	# TLS / Cipher conf
    SECURITY_DATA=$(echo Q | openssl s_client -connect "$dns_name:443" 2> /dev/null | grep New,)
	TLS=$(echo "$SECURITY_DATA" | cut -d"," -f2)
	USED_CIPHER=$(echo "$SECURITY_DATA" | cut -d"," -f3 | cut -d" " -f4-)

	ADDED_CIPHER="{ \
\"BROKER_TLS13_CIPHER_SUITES\" : \"$(grep BROKER_TLS13_CIPHER_SUITES < .env | cut -d"=" -f2)\", \
\"BROKER_TLS12_CIPHER_SUITES\" : \"$(grep BROKER_TLS12_CIPHER_SUITES < .env | cut -d"=" -f2)\", \
\"PROXY_TLS13_CIPHER_SUITES\" : \"$(grep PROXY_TLS13_CIPHER_SUITES < .env | cut -d"=" -f2)\", \
\"PROXY_TLS12_CIPHER_SUITES\" : \"$(grep PROXY_TLS12_CIPHER_SUITES < .env | cut -d"=" -f2)\"}"

	RES=$(jq --null-input "${RES} + {\"TLS\":\"${TLS}\",\"UsedCipher\":\"${USED_CIPHER}\", AddedCipher:$ADDED_CIPHER}")
	
	echo "{\"siteInfo\" :${RES}}"
}

function get_conf_data() {
	RES="{}"

	# rtu blacklist 
	BLACKLISTED_RTU_IDS=$(grep REMOTE_LOG_BLACKLIST < .env | cut -d"=" -f2 | tr "," " ")
	declare -a data_list
	read -ra data_list <<< "$BLACKLISTED_RTU_IDS"
	if [ -n "$BLACKLISTED_RTU_IDS" ]; then
		OPP=$(printf "+[\"%s\"]" "${data_list[@]}")
		BLACKLISTED_RTU_IDS=$(jq --null-input "[] ${OPP}")
	else
		BLACKLISTED_RTU_IDS="[]"
	fi

	RES=$(jq --null-input "${RES} + {\"blacklistedRtuIds\": ${BLACKLISTED_RTU_IDS}}")

	# mutual auth
	MUTUAL_AUTH_ACTIVED=$(grep RTU_SSL_AUTH_REQUIRED < .env | cut -d"=" -f2)
	if [ -n "$MUTUAL_AUTH_ACTIVED" ]; then
	RES=$(jq --null-input "${RES} + {\"mutualAuthActived\":${MUTUAL_AUTH_ACTIVED}}")	
	else
		RES=$(jq --null-input "${RES} + {\"mutualAuthActived\":\"false\"}")
	fi

	# Add res to json
	RES=$(jq -n "$RES + {secretsNames : $(docker secret ls --format "{{json .Name}}" | jq -s)}")
	echo "${RES}"
}

function check_secret () {

	CHECKED_SECRETS="[]"

	declare -a secret_names
	secret_names=(
		"SERVER_CERT_SECRET"
		"SERVER_CERT_KEY_SECRET"
		"PROXY_SESSION_SECRET"
		"TOPOLOGY_DB_PASSWORD_SECRET"
		"DWH_DB_PASSWORD_SECRET"
		"KEYCLOAK_DB_PASSWORD_SECRET"
		"MONGO_PASSWORD_MEASURE_SECRET"
		"MONGO_PASSWORD_ALARMING_SECRET"
		"MONGO_PASSWORD_ALARMING_METRICS_SECRET"
		"MONGO_PASSWORD_RTU_API_GATEWAY_SECRET"
		"CONDUCTOR_DB_PASSWORD_SECRET"
		"RTU_VERSION_CONTROLLER_TOKEN_SECRET"
		"JOLOKIA_PASSWORD_SECRET"
		"RTU_BROKER_ADMIN_PASSWORD_SECRET"
		"IAM_CLIENT_SECRET"
		"IAM_SERVICE_AUTHORIZATION_PROXY_CLIENT_SECRET"
		"KEYCLOAK_MASTER_ADMIN_USER_INIT_SECRET"
		"KEYCLOAK_FIBER_ADMIN_USER_INIT_SECRET"
		"KEYCLOAK_FIBER_TEST_USER_INIT_SECRET"
	)
	
	if [ "$replication" == "true" ]; then
		secret_names+=(
			"MONGO_MEASURE_REPLICATION_TOKEN_SECRET"
			"MONGO_ALARM_REPLICATION_TOKEN_SECRET"
			"FILE_REPLICATION_SECRET"
		)
	fi

	# gather all var values
	for var_name in "${secret_names[@]}" ; do
		value=$(awk -F= "\$1 == \"$var_name\" {print \$2}" .env)

		# check if var is present
		if [ -z "$value" ]; then
			CHECKED_SECRETS=$(jq --null-input "$CHECKED_SECRETS + [\"$var_name is not present in .env\"]")
			continue
		fi

		# check if value is as expected
		if [  -z "$(docker secret ls --format "{{json .Name}}" | tr -d "\"" | grep "$value$" && echo "ok" || echo "")" ] ; then
			CHECKED_SECRETS=$(jq --null-input "$CHECKED_SECRETS + [\"$var_name value does'nt correspond to docker value\"]")
		fi
	done

	echo "$CHECKED_SECRETS"
}

function check_dns_variables () {

	CHECKED_DNS_NAME="[]"

	dns_var=(
		"ROOT_DNS|"
		"TOPOLOGY_API_DNS|api."
		"ALARM_DNS|alarm."
		"KEYCLOAK_DNS|auth."
		"TOPOLOGY_UI_DNS|"
		"DATA_DNS|data."
		"MESSAGE_DNS|message."
		"IAM_SERVER|auth."
	)

	# gather all var values
	for item in "${dns_var[@]}" ; do
		var_name=$(echo "$item" | cut -d "|" -f1)
		value=\"$(awk -F= "\$1 == \"$var_name\" {print \$2}" .env)\"

		# check if var exist
		if [ -z "$value" ]; then
			CHECKED_DNS_NAME=$(jq --null-input "$CHECKED_DNS_NAME + [\"$var_name is not present in .env\"]")
			continue
		fi
		
		# check if value is as expected
		check_value="\"$(echo "$item" | cut -d"|" -f2)$dns_name\""
		if [ "$value" != "$check_value" ] ; then
			CHECKED_DNS_NAME=$(jq --null-input "$CHECKED_DNS_NAME + [\"$var_name value does'nt correspond to expected value\"]")
		fi
	done

	echo "$CHECKED_DNS_NAME"
}

function check_others_variables_names {

	CHECKED_VARS="[]"

	var_names+=(
		"ORCHESTRATOR"
		"KEYCLOAK_MASTER_ADMIN_USER_INIT"
		"KEYCLOAK_FIBER_ADMIN_USER_INIT"
		"KEYCLOAK_FIBER_TEST_USER_INIT"
	)
	
	# gather var values
	for var_name in "${var_names[@]}" ; do
		value=$(awk -F= "\$1 == \"$var_name\" {print \$2}" .env)

		# check if var exist
		if [ -z "$value" ]; then
			CHECKED_VARS=$(jq --null-input "$CHECKED_VARS + [\"$var_name is not present in .env\"]")
		fi
	done

	echo "$CHECKED_VARS"
}

function check_env() {

	# partially done by swarm.sh

	RES="{}"
	dns_name=$(grep ROOT_DNS < .env | cut -d "=" -f2 | tr -d "\"")

	# check if replication is enabled

	replication=$(awk -F= "\$1 == \"REPLICATION_ENABLED\" {print \$2}" .env)
	if [ -z "$replication" ] ; then
		replication="false"
	fi

	RES=$(jq --null-input "$RES + {\"replication\":\"$replication\"}" )
	CHECKED_VARS="[]"
	
	CHECKED_VARS=$(jq --null-input "$CHECKED_VARS + $(check_secret)")
	CHECKED_VARS=$(jq --null-input "$CHECKED_VARS + $(check_dns_variables)")
	CHECKED_VARS=$(jq --null-input "$CHECKED_VARS + $(check_others_variables_names)")
	
	RES=$(jq --null-input "$RES + {\"envDataCheck\" : $CHECKED_VARS}")
	echo "$RES"
}

function env_check() {

	DATA=$(./swarm.sh --list-usefull-env)
	echo "{\"envCheck\":\"$DATA\"}"
}

function check_files () {

	RES="{}"

	# check if docker-compose-local.yml is present
	DOCKER_COMPOSE_LOCAL=$(find . -name docker-compose-local.yml)

	# check if promethesu.yml is present
	PROMETHEUS=$(find ../config -name prometheus.yml)

	# get addons list
	declare -a ADDONS_LIST
	ADDONS=$(find ./ -maxdepth 1 -name "*.addon" | rev | cut -d"/" -f1 | rev | tr "\n" " ")

	ADDONS="${ADDONS//.addon/}"	# delete .addons at the end of each lines
	read -ra ADDONS_LIST <<< "$ADDONS"
	ADDONS=$(printf "+[\"%s\"]" "${ADDONS_LIST[@]}")
	ADDONS=$(jq -n "[] $ADDONS")
	RES=$(jq --null-input "$RES+{\"prometheus\":\"$PROMETHEUS\",\"dockerComposeLocal\":\"$DOCKER_COMPOSE_LOCAL\", \"addons\":$ADDONS}")

	# get docker-compose.yml md5sum

	SWARM_MD5SUM=$(md5sum ./swarm.sh | cut -d" " -f1)
	RES=$(jq --null-input "$RES+{\"swarmMD5SUM\":\"$SWARM_MD5SUM\"}")

    echo "$RES"

}

function check_config_overide() {

	RES="{}"

	# get all config files name

	FILES=$(find ../config -type f | tr "\n" " ")

	declare -a FILE_LIST
	read -ra FILES_LIST <<< "$FILES"

	# format in json

	FILES=$(printf "+[\"%s\"]" "${FILES_LIST[@]}")
	FILES=$(jq --null-input "[] $FILES")
	RES=$(jq --null-input "$RES + {\"configFileOverride\":$FILES}")

	echo "$RES"
}

function check_smtp_server() {

	secret_var_name=$(grep ALARMING_CONFIG_EMAIL_FILE_SECRET < .env)

	# check if json in /opt/fms/solution/config/alarms
	if [ -f "/opt/fms/solution/config/alarms/email-notification-config.json" ]; then
		read -ra DATA <<< "$(jq -r ".SMTP_HOST, .SMTP_PORT" /opt/fms/solution/config/alarms/email-notification-config.json | tr "\n" " ")"
	
	# check if in secret
	elif [ "$secret_var_name" ]; then
		read -ra DATA <<< "$(docker exec "$(docker ps -qf "name=^fms_alarming.1")" cat /run/secrets/email-notification-config.json | jq -r ".SMTP_HOST, .SMTP_PORT" | tr "\n" " ")"
	fi

	HOST="null"
	PORT="null"

	if [ -n "${DATA+x}" ] && [ -n "${DATA[0]+x}" ] && [ -n "${DATA[1]+x}" ]; then
		HOST="${DATA[0]}"
		PORT="${DATA[1]}"
	fi

	# netcat host
	if [ "$HOST" != "null" ] && [ "$PORT" != "null" ]; then
		OUTPUT=$(docker exec "$(docker ps -qf "name=^fms_alarming.1")" nc -v -w1 "$HOST" "$PORT" 2> /dev/null)
		
		# check error
		EXIT_CODE=$?
		if [[ $EXIT_CODE != 0 ]]; then
			OUTPUT="netcat exited with error code : $EXIT_CODE"
		fi
	else
		OUTPUT="No smtp server found"
	fi

	# res

	echo "{\
\"smtpHost\" : \"$HOST:$PORT\", \
\"smtpServerState\":\"$OUTPUT\" }"
}

function check_snmp_server() {

	RES=""
	secret_var_name=$(grep ALARMING_CONFIG_SNMP_FILE_SECRET < .env)

	# check if json in /opt/fms/solution/config/alarms
	if [ -f "/opt/fms/solution/config/alarms/snmp-notification-config.json" ]; then
		read -ra DATA <<< "$(jq -r " .[] | .snmpReceiver, .snmpPort" /opt/fms/solution/config/alarms/snmp-notification-config.json | tr "\n" " ")"
	
	# check if in secret
	elif [ "$secret_var_name" ]; then
		read -ra DATA <<< "$(docker exec "$(docker ps -qf "name=^fms_alarming.1")" cat /run/secrets/snmp-notification-config.json | jq -r ".SNMP_HOST, .SNMP_PORT" | tr "\n" " ")"
	fi

	# checking host connexion
	HOST="null"
	PORT="null"

	if [ -n "${DATA+x}" ] && [ -n "${DATA[0]+x}" ] && [ -n "${DATA[1]+x}" ]; then
		HOST="${DATA[0]}"
		PORT="${DATA[1]}"

		# trying to connect with netcat
		OUTPUT=$(docker exec "$(docker ps -qf "name=^fms_alarming.1")" nc -v -w1 "$HOST" "$PORT" 2> /dev/null)

		# check error
		EXIT_CODE=$?
		if [[ $EXIT_CODE != 0 ]]; then
			OUTPUT="netcat exited with error code : $EXIT_CODE"
		fi

		# adding datat to res
		RES="$RES{\
\"host\" : \"$HOST:$PORT\", \
\"serverState\":\"$OUTPUT\" }"

	else
		OUTPUT="No smtp server found"
	fi

	# res
	RES=$(echo "$RES" | jq -s )
	echo "{\"snmpServer\" : $RES}"
}

function check_ca_certificate() {

	# getting CA certificate
	dns_name=$(grep ROOT_DNS < .env | cut -d "=" -f2 | tr -d "\"")
	file=$(mktemp)
	echo "Q" | openssl s_client -showcerts -connect "$dns_name:443" 2> /dev/null | sed -n '/^-----BEGIN CERT/,/^-----END CERT/p' > "$file"
	if [ -s "$file" ]; then
		# gathering data from certificate

		CERT_INFO=$(awk -F'\n' 'BEGIN {
			showcert = "openssl x509 -noout -dates -subject -issuer"
		}
		/-----BEGIN CERTIFICATE-----/ {
			printf "====\n"}{printf $0"\n" | showcert
		}
		/-----END CERTIFICATE-----/ {
			close(showcert) ind ++
		}
		END {
		printf "end"}' "$file")

		# json formating
		STATUS=$?
		if [ $STATUS == 0 ]; then
			CERT_INFO=$(echo "$CERT_INFO" | sed -e 's/notBefore=/"notBefore":"/g' -e 's/notAfter=/"notAfter":"/g' -e 's/subject=/"subject":"/g' -e 's/issuer=/"issuer":"/g' -ze 's/\n/",\n/g' -ze 's/,\n====",\n/}{/g' -ze 's/====",\n/{/g' -ze 's/,\nend",/}/g' | jq -s)
		else
			CERT_INFO="Error while reading certificate"
		fi
		
		# check the chain validity
		CERT_CHAIN_VALIDITY=$(openssl verify -untrusted  "$file" "$file")
		
		# final result
		echo "{\"certInfo\":$CERT_INFO, \"certChainValidity\":\"$CERT_CHAIN_VALIDITY\"}"
	else
		echo "{\"certInfo\":\"No certificate\", \"certChainValidity\":\"No certificate\"}"
	fi
}
