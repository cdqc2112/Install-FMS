#!/bin/bash

# Basic setup script for docker.

#set -euo pipefail

echo "Installing docker"

source ./multi-os.sh
source /etc/os-release
source ./setup.cfg

MOUNT_POINT=/var/lib/docker
DISK_DEVICE=/dev/sdc

read -r -p 'Is there a free volume for Docker? [y/N] ' response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then
    lsblk
    read -rep "Confirm the disk to be used (will be erased): " -i ${DISK_DEVICE} DISK_DEVICE
    parted ${DISK_DEVICE} mklabel msdos
    parted -m -s ${DISK_DEVICE} unit mib mkpart primary 1 100%
    sleep 2
    pvcreate ${DISK_DEVICE}1
    vgcreate docker_vg ${DISK_DEVICE}1
    lvcreate -l 100%VG -n docker docker_vg
    mkfs.xfs /dev/docker_vg/docker
    UUID=`blkid -o export /dev/docker_vg/docker | grep UUID | grep -v PARTUUID`
    mkdir ${MOUNT_POINT}
    echo "${UUID}  ${MOUNT_POINT}    xfs    defaults 0 0" >> /etc/fstab
    mount -avt xfs
fi

function iptables() {
if [ $(dpkg-query -W -f='${Status}' iptables 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  cd $WORKINGDIR/iptables/
  dpkg -i *.deb
fi
}

function configDocker() {
    mkdir -p /etc/docker

cat > /etc/docker/daemon.json <<EOF
{
    "log-driver":"json-file",
    "log-opts":{
        "max-size": "10M",
        "max-file": "10"
    }
}
EOF

}

if [ "$INSTALL_OPTION" = "offline" ];then
# offline
    if [ "$ID_LIKE" = "debian" ]; then
        iptables
        configDocker
        apt remove -y docker docker-engine docker.io || true 
        cd $WORKINGDIR/docker/
        dpkg -i ./containerd.io_1.6.33-1_amd64.deb
        #Ubuntu Focal
        #if [ "$UBUNTU_CODENAME" = "focal" ]; then
        #    dpkg -i ./docker-buildx-plugin_0.11.2-1~ubuntu.20.04~${UBUNTU_CODENAME}_amd64.deb \
        #    ./docker-compose-plugin_2.21.0-1~ubuntu.20.04~${UBUNTU_CODENAME}_amd64.deb
        #    if [ "$TARGET_DOCKER_VERSION" = "20" ]; then
        #        dpkg -i ./docker-ce_20.10.24~3-0~ubuntu-${UBUNTU_CODENAME}_amd64.deb \
        #        ./docker-ce-cli_20.10.24~3-0~ubuntu-${UBUNTU_CODENAME}_amd64.deb
        #    elif [ "$TARGET_DOCKER_VERSION" = "24" ]; then
        #        dpkg -i ./docker-ce_24.0.7-1~ubuntu.20.04~${UBUNTU_CODENAME}_amd64.deb \
        #        ./docker-ce-cli_24.0.7-1~ubuntu.20.04~${UBUNTU_CODENAME}_amd64.deb
        #    fi
        #Ubuntu Jammy
        if [ "$UBUNTU_CODENAME" = "jammy" ]; then
            dpkg -i ./docker-buildx-plugin_0.11.2-1~ubuntu.22.04~${UBUNTU_CODENAME}_amd64.deb \
            ./docker-compose-plugin_2.21.0-1~ubuntu.22.04~${UBUNTU_CODENAME}_amd64.deb
            if [ "$TARGET_DOCKER_VERSION" = "20" ]; then
                dpkg -i ./docker-ce-cli_20.10.24~3-0~ubuntu-${UBUNTU_CODENAME}_amd64.deb \
                ./docker-ce_20.10.24~3-0~ubuntu-${UBUNTU_CODENAME}_amd64.deb
            elif [ "$TARGET_DOCKER_VERSION" = "24" ]; then
                dpkg -i ./docker-ce-cli_24.0.7-1~ubuntu.22.04~${UBUNTU_CODENAME}_amd64.deb \
                ./docker-ce_24.0.7-1~ubuntu.22.04~${UBUNTU_CODENAME}_amd64.deb                   
            fi
        #Ubuntu Noble
        elif [ "$UBUNTU_CODENAME" = "noble" ]; then
            dpkg -i ./docker-buildx-plugin_0.14.1-1~ubuntu.24.04~${UBUNTU_CODENAME}_amd64.deb \
            ./docker-compose-plugin_2.27.1-1~ubuntu.24.04~${UBUNTU_CODENAME}_amd64.deb
            if [ "$TARGET_DOCKER_VERSION" = "24" ]; then
                echo "Docker 24 is not available for ${UBUNTU_CODENAME}. Installing Docker 26 instead" 
                dpkg -i ./docker-ce-cli_26.1.4-1~ubuntu.24.04~${UBUNTU_CODENAME}_amd64.deb \
                ./docker-ce_26.1.4-1~ubuntu.24.04~${UBUNTU_CODENAME}_amd64.deb
            elif [ "$TARGET_DOCKER_VERSION" = "26" ]; then
               dpkg -i ./docker-ce-cli_26.1.4-1~ubuntu.24.04~${UBUNTU_CODENAME}_amd64.deb \
                ./docker-ce_26.1.4-1~ubuntu.24.04~${UBUNTU_CODENAME}_amd64.deb                            
            fi
        fi
        cd $WORKINGDIR
    else
        configDocker
        yum remove docker docker-client docker-client-latest docker-commondocker-logrotate docker-engine
        yum remove -y firewalld
        if which iptables > /dev/null 2>&1 ; then
            service iptables restart
            # Warning: SE linux is known to cause troubles as well

            # Cleanup firewall for leftovers...
            iptables -F
            iptables -t nat -F
        fi
        cd $WORKINGDIR/docker/
        $FMS_INSTALLER install -y *.rpm
        cd $WORKINGDIR
    fi
else
# online
    if [ "$ID_LIKE" = "debian" ]; then

        apt remove -y docker docker-engine docker.io || true

        configDocker

        apt install -y apt-transport-https ca-certificates curl software-properties-common

        addAptGPGKey docker https://download.docker.com/linux/ubuntu/gpg
        add-apt-repository  "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
        apt update -y

        DOCKER_PREFIX="`apt-cache madison docker-ce | cut -d '|' -f 2 | grep -a -o '[0-9][:]'$TARGET_DOCKER_VERSION'\.' | head -n 1`"

        if [ -z "$DOCKER_PREFIX" ]; then
            echo "No docker version found - aborting"
            exit 1
        fi

    cat > /etc/apt/preferences.d/20-docker-major-pin <<EOF
# Stick to docker version ${TARGET_DOCKER_VERSION}
Package: docker-ce*
Pin: version ${DOCKER_PREFIX}*
Pin-Priority: 1001

# Prevent accidental install of docker.io
Package: docker.io
Pin-Priority: -1
EOF

        echo "Installing version $DOCKER_PREFIX*"
        for I in 1 2 3 4; do
            DOCKER_INSTALL_SUCCESS=true
            apt-get install -y --allow-downgrades  "docker-ce" "docker-ce-cli" containerd.io && break
            DOCKER_INSTALL_SUCCESS=false
        done

        if [ "$DOCKER_INSTALL_SUCCESS" = false ]; then
            echo "Unable to install docker - aborting"
            exit 1
        fi

    else
        yum remove docker docker-client docker-client-latest docker-commondocker-logrotate docker-engine
        yum install -y yum-utils
        # Firewalld does not play well with docker - OMG
        yum remove -y firewalld

        if which iptables > /dev/null 2>&1 ; then
            service iptables restart
            # Warning: SE linux is known to cause troubles as well

            # Cleanup firewall for leftovers...
            iptables -F
            iptables -t nat -F
        fi

        configDocker

        # Install docker version from TARGET_DOCKER_VERSION
        yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

        DOCKER_VERSION="`yum list docker-ce --showduplicates | sort -r | cut -d ':' -f 2 | cut -d '-' -f 1 | sort  -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | grep -aE "^$TARGET_DOCKER_VERSION\." | tail -1`"

        if [ -z "$DOCKER_VERSION" ]; then
            echo "No docker version found - aborting"
            exit 1
        fi

        if [[ "$NAME" =~ 'Oracle Linux '* ]]; then
            # Addon is required for dependencies of docker ce for oracle
            yum-config-manager --enable '*addons'

            # Install docker version for ORACLE
            for I in 1 2 3 4; do
                DOCKER_INSTALL_SUCCESS=true
                yum install --skip-broken -y "docker-ce-$DOCKER_VERSION" "docker-ce-cli-$DOCKER_VERSION" containerd.io && break
                DOCKER_INSTALL_SUCCESS=false
            done
            #if [["$TARGET_DOCKER_VERSION" >= 20 ]] , then yum -y install slirp4netns fuse-overlayfs container-selinux

        # Install docker version for CENTOS and Others
        else
            for I in 1 2 3 4; do
                DOCKER_INSTALL_SUCCESS=true
                yum install -y "docker-ce-$DOCKER_VERSION" "docker-ce-cli-$DOCKER_VERSION" containerd.io && break
                yum versionlock "docker-ce-$DOCKER_VERSION" "docker-ce-cli-$DOCKER_VERSION" containerd.io && break 
                DOCKER_INSTALL_SUCCESS=false
            done

            if [ "$DOCKER_INSTALL_SUCCESS" = false ]; then
                echo "Unable to install docker - aborting"
                exit 1
            fi

        fi
    fi
fi
#    yum versionlock docker-*

# For builtin version (buggy with at least with image dl)

sed -i -e 's/^\(After=.*\)\( local-fs.target\| remote-fs.target\)/\1/g' \
    -e 's/^\(After=.*\)\( local-fs.target\| remote-fs.target\)/\1/g' \
    -e 's/\(After=.*\)/\1 local-fs.target remote-fs.target/' /usr/lib/systemd/system/docker.service


systemctl daemon-reload
systemctl enable docker
systemctl start docker

if which iptables > /dev/null 2>&1 ; then
    iptables -I INPUT -p tcp --dport 2376 -j ACCEPT
    iptables -I INPUT -p tcp --dport 2377 -j ACCEPT
    iptables -I INPUT -p tcp --dport 7946 -j ACCEPT
    iptables -I INPUT -p udp --dport 7946 -j ACCEPT
    iptables -I INPUT -p udp --dport 500 -j ACCEPT
    iptables -I INPUT -p esp -j ACCEPT

    #service iptables restart
fi

clear

if [[ -z "$DOCKERHUB_PASS" ]]; then
    echo "Enter password for user $DOCKERHUB_USER to login on dockerhub"
    docker login -u $DOCKERHUB_USER
else
    docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASS
fi