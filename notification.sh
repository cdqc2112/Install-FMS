#! /bin/bash
#set -euxo pipefail
# Set or renew email and SNMP config files and secrets
WORKINGDIR=${PWD}
EMAIL_FILE="email-notification-config.json"
SNMP_FILE="snmp-notification-config.json"
source /opt/fms/solution/deployment/.env
# Email
# Check if secret exists int .env
if grep ALARMING_CONFIG_EMAIL_FILE_SECRET= /opt/fms/solution/deployment/.env;then
	SMTP_SECRET=$ALARMING_CONFIG_EMAIL_FILE_SECRET
	sed -i 's/ALARMING_CONFIG_EMAIL_FILE_SECRET='${SMTP_SECRET}'/ALARMING_CONFIG_EMAIL_FILE_SECRET=/g' /opt/fms/solution/deployment/.env
	docker service rm fms_alarming fms_secrets-exporter
	docker secret rm $SMTP_SECRET
else
	SMTP_SECRET="ALARMING_CONFIG_EMAIL_FILE_SECRET"
	printf "ALARMING_CONFIG_EMAIL_FILE_SECRET=\n" >> /opt/fms/solution/deployment/.env
fi
# Check if file exists
if [ ! -f "/opt/fms/solution/config/alarms/${EMAIL_FILE}" ];then
	cp $WORKINGDIR/deployment/config/alarms/$EMAIL_FILE /opt/fms/solution/config/alarms
fi

# Get values from existing file
cd /opt/fms/solution/config/alarms

SMTP_HOST=$(awk -F'"' '/SMTP_HOST/ {print$4}' $EMAIL_FILE)
SMTP_PORT=$(awk -F'"' '/SMTP_PORT/ {print$4}' $EMAIL_FILE)
SMTP_SECURE=$(awk -F'"' '/SMTP_SECURE/ {print$4}' $EMAIL_FILE)
SMTP_USER=$(awk -F'"' '/SMTP_USER/ {print$4}' $EMAIL_FILE)
SMTP_HELO=$(awk -F'"' '/SMTP_HELO/ {print$4}' $EMAIL_FILE)
MAIL_FROM=$(awk -F'"' '/MAIL_FROM/ {print$4}' $EMAIL_FILE)
MAIL_TO=$(awk -F'"' '/MAIL_TO/ {print$4}' $EMAIL_FILE)
IS_NOTIFY=$(awk -F'"' '/IS_NOTIFY/ {print$4}' $EMAIL_FILE)
IS_SOR_ATTACH=$(awk -F'"' '/IS_SOR_ATTACH/ {print$4}' $EMAIL_FILE)

# Input user's values
echo
echo "Configuring SMTP server"
echo
read -e -p "Enter the SMTP server's address or hostname: " -i $SMTP_HOST SMTP_HOST
read -e -p "Enter the SMTP port number (25, 465): " -i $SMTP_PORT SMTP_PORT
read -e -p "SMTP secure required (true or false): " -i $SMTP_SECURE SMTP_SECURE
if [ -z $SMTP_USER ];then
	read -e -p "Enter the SMTP username: " SMTP_USER
else
	read -e -p "Enter the SMTP username: " -i $SMTP_USER SMTP_USER
fi
read -e -p "Enter the SMTP password: " -i "password" SMTP_PASSWORD
if [ -z $SMTP_HELO ];then
	read -e -p "Enter the SMTP helo (ex.: HELO client.example.com): " SMTP_HELO
else
	read -e -p "Enter the SMTP helo (ex.: HELO client.example.com): " -i $SMTP_HELO SMTP_HELO
fi
if [ -z $MAIL_FROM ];then
	read -e -p "Enter the from address: " MAIL_FROM
else
	read -e -p "Enter the from address: " -i $MAIL_FROM MAIL_FROM
fi
if [ -z $MAIL_TO ];then
	read -e -p "Enter the  destination address: " MAIL_TO
else
	read -e -p "Enter the  destination address: " -i $MAIL_TO MAIL_TO
fi
read -e -p "Notification (true or false): " -i $IS_NOTIFY IS_NOTIFY
read -e -p "SOR file attached (true or false): " -i $IS_SOR_ATTACH IS_SOR_ATTACH
echo
cat > $WORKINGDIR/$EMAIL_FILE <<EOF
{
    "SMTP_HOST" : "$SMTP_HOST",
    "SMTP_PORT" : "$SMTP_PORT",
    "SMTP_SECURE" : "$SMTP_SECURE",
    "SMTP_USER" : "$SMTP_USER",
    "SMTP_PASSWORD" : "$SMTP_PASSWORD",
    "SMTP_HELO" : "$SMTP_HELO",
    "MAIL_FROM" : "$MAIL_FROM",
    "MAIL_TO" : "$MAIL_TO",
    "IS_NOTIFY" : "$IS_NOTIFY",
    "IS_SOR_ATTACH": "$IS_SOR_ATTACH"
}
EOF

cp $WORKINGDIR/$EMAIL_FILE /opt/fms/solution/config/alarms

docker secret create $SMTP_SECRET /opt/fms/solution/config/alarms/$EMAIL_FILE
sed -i 's/ALARMING_CONFIG_EMAIL_FILE_SECRET=/ALARMING_CONFIG_EMAIL_FILE_SECRET='${SMTP_SECRET}'/g' /opt/fms/solution/deployment/.env

# SNMP
# Check if secret exists int .env

read -r -p 'Are you configuring SNMP? [y/N] ' response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then

	if grep ALARMING_CONFIG_SNMP_FILE_SECRET= /opt/fms/solution/deployment/.env;then
		SNMP_SECRET=$ALARMING_CONFIG_SNMP_FILE_SECRET
		sed -i 's/ALARMING_CONFIG_SNMP_FILE_SECRET='${SNMP_SECRET}'/ALARMING_CONFIG_SNMP_FILE_SECRET=/g' /opt/fms/solution/deployment/.env
		docker service rm fms_alarming fms_secrets-exporter
		docker secret rm $SNMP_SECRET
	else
		SNMP_SECRET="ALARMING_CONFIG_SNMP_FILE_SECRET"
		printf "ALARMING_CONFIG_SNMP_FILE_SECRET=\n" >> /opt/fms/solution/deployment/.env
	fi
	# Check if file exists
	if [ ! -f "/opt/fms/solution/config/alarms/${SNMP_FILE}" ];then
		cp $WORKINGDIR/deployment/config/alarms/$SNMP_FILE /opt/fms/solution/config/alarms
	fi

	# Get values from existing file
	cd /opt/fms/solution/config/alarms

	SNMP_RECEIVER=$(awk -F'"' '/snmpReceiver/ {print$4}' $SNMP_FILE)
	SNMP_PORT=$(awk '/snmpPort/ {print$3}' $SNMP_FILE)
	SNMP_VERSION=$(awk -F'"' '/snmpVersion/ {print$4}' $SNMP_FILE)
	SNMP_ENGINE_ID=$(awk -F'"' '/snmpEngineId/ {print$4}' $SNMP_FILE)
	SNMP_AUTH_USER=$(awk -F'"' '/snmpAuthUser/ {print$4}' $SNMP_FILE)
	SNMP_AUTH_PROTOCOL=$(awk -F'"' '/snmpAuthProtocol/ {print$4}' $SNMP_FILE)
	SNMP_PRIV_PROTOCOL=$(awk -F'"' '/snmpPrivProtocol/ {print$4}' $SNMP_FILE)
	SNMP_COMMUNITY=$(awk -F'"' '/snmpCommunity/ {print$4}' $SNMP_FILE)
	IS_SNMP_NOTIFY=$(awk -F'"' '/IS_SNMP_NOTIFY/ {print$4}' $SNMP_FILE)

	# Set variables values

	SNMP_RECEIVER1=$(echo $SNMP_RECEIVER | cut -f1 -d' ' )
	SNMP_RECEIVER2=$(echo $SNMP_RECEIVER | cut -f2 -d' ' )
	SNMP_RECEIVER3=$(echo $SNMP_RECEIVER | cut -f3 -d' ' )
	SNMP_RECEIVER4=$(echo $SNMP_RECEIVER | cut -f4 -d' ' )
	SNMP_PORT1=$(echo $SNMP_PORT | cut -f1 -d' ' )
	SNMP_PORT1=${SNMP_PORT1::-1}
	SNMP_PORT2=$(echo $SNMP_PORT | cut -f2 -d' ' )
	SNMP_PORT2=${SNMP_PORT2::-1}
	SNMP_PORT3=$(echo $SNMP_PORT | cut -f3 -d' ' )
	SNMP_PORT3=${SNMP_PORT3::-1}
	SNMP_PORT4=$(echo $SNMP_PORT | cut -f4 -d' ' )
	SNMP_PORT4=${SNMP_PORT4::-1}
	SNMP_VERSION1=$(echo $SNMP_VERSION | cut -f1 -d' ' )
	SNMP_VERSION2=$(echo $SNMP_VERSION | cut -f2 -d' ' )
	SNMP_VERSION3=$(echo $SNMP_VERSION | cut -f3 -d' ' )
	SNMP_VERSION4=$(echo $SNMP_VERSION | cut -f4 -d' ' )
	SNMP_ENGINE_ID1=$(echo $SNMP_ENGINE_ID | cut -f1 -d' ' )
	SNMP_ENGINE_ID2=$(echo $SNMP_ENGINE_ID | cut -f2 -d' ' )
	SNMP_ENGINE_ID3=$(echo $SNMP_ENGINE_ID | cut -f3 -d' ' )
	SNMP_ENGINE_ID4=$(echo $SNMP_ENGINE_ID | cut -f4 -d' ' )
	SNMP_AUTH_USER1=$(echo $SNMP_AUTH_USER | cut -f1 -d' ' )
	SNMP_AUTH_USER2=$(echo $SNMP_AUTH_USER | cut -f2 -d' ' )
	SNMP_AUTH_USER3=$(echo $SNMP_AUTH_USER | cut -f3 -d' ' )
	SNMP_AUTH_USER4=$(echo $SNMP_AUTH_USER | cut -f4 -d' ' )
	SNMP_AUTH_PROTOCOL1=$(echo $SNMP_AUTH_PROTOCOL | cut -f1 -d' ' )
	SNMP_AUTH_PROTOCOL2=$(echo $SNMP_AUTH_PROTOCOL | cut -f2 -d' ' )
	SNMP_AUTH_PROTOCOL3=$(echo $SNMP_AUTH_PROTOCOL | cut -f3 -d' ' )
	SNMP_AUTH_PROTOCOL4=$(echo $SNMP_AUTH_PROTOCOL | cut -f4 -d' ' )
	SNMP_PRIV_PROTOCOL1=$(echo $SNMP_PRIV_PROTOCOL | cut -f1 -d' ' )
	SNMP_PRIV_PROTOCOL2=$(echo $SNMP_PRIV_PROTOCOL | cut -f2 -d' ' )
	SNMP_PRIV_PROTOCOL3=$(echo $SNMP_PRIV_PROTOCOL | cut -f3 -d' ' )
	SNMP_PRIV_PROTOCOL4=$(echo $SNMP_PRIV_PROTOCOL | cut -f4 -d' ' )
	SNMP_COMMUNITY1=$(echo $SNMP_COMMUNITY | cut -f1 -d' ' )
	SNMP_COMMUNITY2=$(echo $SNMP_COMMUNITY | cut -f2 -d' ' )
	SNMP_COMMUNITY3=$(echo $SNMP_COMMUNITY | cut -f3 -d' ' )
	SNMP_COMMUNITY4=$(echo $SNMP_COMMUNITY | cut -f4 -d' ' )
	IS_SNMP_NOTIFY1=$(echo $IS_SNMP_NOTIFY | cut -f1 -d' ' )
	IS_SNMP_NOTIFY2=$(echo $IS_SNMP_NOTIFY | cut -f2 -d' ' )
	IS_SNMP_NOTIFY3=$(echo $IS_SNMP_NOTIFY | cut -f3 -d' ' )
	IS_SNMP_NOTIFY4=$(echo $IS_SNMP_NOTIFY | cut -f4 -d' ' )

	# Input user's values
	echo
	echo "Configuring SNMP 1 of 4"
	echo
	if [ -z $SNMP_RECEIVER1 ];then
		read -e -p "Enter the SNMP server's address or hostname 1/4: " SNMP_RECEIVER1
	else
		read -e -p "Enter the SNMP server's address or hostname 1/4: " -i $SNMP_RECEIVER1 SNMP_RECEIVER1
	fi
	read -e -p "Enter the SNMP port 1/4: " -i $SNMP_PORT1 SNMP_PORT1
	read -e -p "Enter the SNMP version 1/4 (v1, v2c or v3): " -i $SNMP_VERSION1 SNMP_VERSION1
	if [ $SNMP_VERSION1 = "v3" ];then
		read -e -p "Enter the SNMP engine ID 1/4: " -i $SNMP_ENGINE_ID1 SNMP_ENGINE_ID1
		read -e -p "Enter the SNMP authorized user 1/4: " -i $SNMP_AUTH_USER1 SNMP_AUTH_USER1
		read -e -p "Enter the SNMP authorized user's password 1/4: " SNMP_AUTH_PASS1
		read -e -p "Enter the SNMP authorized protocol 1/4: " -i $SNMP_AUTH_PROTOCOL1 SNMP_AUTH_PROTOCOL1
		read -e -p "Enter the SNMP private protocol 1/4: " -i $SNMP_PRIV_PROTOCOL1 SNMP_PRIV_PROTOCOL1
		read -e -p "Enter the SNMP private password 1/4: " SNMP_PRIV_PASS1
	fi
	read -e -p "Enter the SNMP community string 1/4: " -i $SNMP_COMMUNITY1 SNMP_COMMUNITY1
	read -e -p "Enabled 1/4 (true or false): " -i $IS_SNMP_NOTIFY1 IS_SNMP_NOTIFY1
	echo
	echo "Configuring SNMP 2 of 4"
	echo
	if [ -z $SNMP_RECEIVER2 ];then
		read -e -p "Enter the SNMP server's address or hostname 2/4: " SNMP_RECEIVER2
	else
		read -e -p "Enter the SNMP server's address or hostname 2/4: " -i $SNMP_RECEIVER2 SNMP_RECEIVER2
	fi
	read -e -p "Enter the SNMP port 2/4: " -i $SNMP_PORT2 SNMP_PORT2
	read -e -p "Enter the SNMP version 2/4 (v1, v2c or v3): " -i $SNMP_VERSION2 SNMP_VERSION2
	if [ $SNMP_VERSION2 = "v3" ];then 
		read -e -p "Enter the SNMP engine ID 2/4: " -i $SNMP_ENGINE_ID2 SNMP_ENGINE_ID2
		read -e -p "Enter the SNMP authorized user 2/4: " -i $SNMP_AUTH_USER2 SNMP_AUTH_USER2
		read -e -p "Enter the SNMP authorized user's password 2/4: " SNMP_AUTH_PASS2
		read -e -p "Enter the SNMP authorized protocol 2/4: " -i $SNMP_AUTH_PROTOCOL2 SNMP_AUTH_PROTOCOL2
		read -e -p "Enter the SNMP private protocol 2/4: " -i $SNMP_PRIV_PROTOCOL2 SNMP_PRIV_PROTOCOL2
		read -e -p "Enter the SNMP private password 2/4: " SNMP_PRIV_PASS2
	fi
	read -e -p "Enter the SNMP community string 2/4: " -i $SNMP_COMMUNITY2 SNMP_COMMUNITY2
	read -e -p "Enabled 2/4 (true or false): " -i $IS_SNMP_NOTIFY2 IS_SNMP_NOTIFY2
	echo
	echo "Configuring SNMP 3 of 4"
	echo
	if [ -z $SNMP_RECEIVER3 ];then
		read -e -p "Enter the SNMP server's address or hostname 3/4: " SNMP_RECEIVER3
	else
		read -e -p "Enter the SNMP server's address or hostname 3/4: " -i $SNMP_RECEIVER3 SNMP_RECEIVER3
	fi
	read -e -p "Enter the SNMP port 3/4: " -i $SNMP_PORT3 SNMP_PORT3
	read -e -p "Enter the SNMP version 3/4 (v1, v2c or v3): " -i $SNMP_VERSION3 SNMP_VERSION3
	if [ $SNMP_VERSION3 = "v3" ];then
		read -e -p "Enter the SNMP engine ID 3/4: " -i $SNMP_ENGINE_ID3 SNMP_ENGINE_ID3
		read -e -p "Enter the SNMP authorized user 3/4: " -i $SNMP_AUTH_USER3 SNMP_AUTH_USER3
		read -e -p "Enter the SNMP authorized user's password 3/4: " SNMP_AUTH_PASS3
		read -e -p "Enter the SNMP authorized protocol 3/4: " -i $SNMP_AUTH_PROTOCOL3 SNMP_AUTH_PROTOCOL3
		read -e -p "Enter the SNMP private protocol 3/4: " -i $SNMP_PRIV_PROTOCOL3 SNMP_PRIV_PROTOCOL3
		read -e -p "Enter the SNMP private password 3/4: " SNMP_PRIV_PASS3
	fi
	read -e -p "Enter the SNMP community string 3/4: " -i $SNMP_COMMUNITY3 SNMP_COMMUNITY3
	read -e -p "Enabled 3/4 (true or false): " -i $IS_SNMP_NOTIFY3 IS_SNMP_NOTIFY3
	echo
	echo "Configuring SNMP 4 of 4"
	echo
	if [ -z $SNMP_RECEIVER4 ];then
		read -e -p "Enter the SNMP server's address or hostname 4/4: " SNMP_RECEIVER4
	else
		read -e -p "Enter the SNMP server's address or hostname 4/4: " -i $SNMP_RECEIVER4 SNMP_RECEIVER4
	fi
	read -e -p "Enter the SNMP port 4/4: " -i $SNMP_PORT4 SNMP_PORT4
	read -e -p "Enter the SNMP version 4/4 (v1, v2c or v3): " -i $SNMP_VERSION4 SNMP_VERSION4
	if [ $SNMP_VERSION4 = "v3" ];then
		read -e -p "Enter the SNMP engine ID 4/4: " -i $SNMP_ENGINE_ID4 SNMP_ENGINE_ID4
		read -e -p "Enter the SNMP authorized user 4/4: " -i $SNMP_AUTH_USER4 SNMP_AUTH_USER4
		read -e -p "Enter the SNMP authorized user's password 4/4: " SNMP_AUTH_PASS4
		read -e -p "Enter the SNMP authorized protocol 4/4: " -i $SNMP_AUTH_PROTOCOL4 SNMP_AUTH_PROTOCOL4
		read -e -p "Enter the SNMP private protocol 4/4: " -i $SNMP_PRIV_PROTOCOL4 SNMP_PRIV_PROTOCOL4
		read -e -p "Enter the SNMP private password 4/4: " SNMP_PRIV_PASS4
	fi
	read -e -p "Enter the SNMP community string 4/4: " -i $SNMP_COMMUNITY4 SNMP_COMMUNITY4
	read -e -p "Enabled 4/4 (true or false): " -i $IS_SNMP_NOTIFY4 IS_SNMP_NOTIFY4

	if [ $SNMP_VERSION1 != "v3" ];then
	
cat > $WORKINGDIR/$SNMP_FILE <<EOF
[
	{
		"snmpReceiver" : "$SNMP_RECEIVER1",
		"snmpPort" : $SNMP_PORT1,
		"snmpVersion" : "$SNMP_VERSION1",
		"snmpCommunity" : "$SNMP_COMMUNITY1",
		"IS_SNMP_NOTIFY" : "$IS_SNMP_NOTIFY1"
	},

	{
		"snmpReceiver" : "$SNMP_RECEIVER2",
		"snmpPort" : $SNMP_PORT2,
		"snmpVersion" : "$SNMP_VERSION2",
		"snmpCommunity" : "$SNMP_COMMUNITY2",
		"IS_SNMP_NOTIFY" : "$IS_SNMP_NOTIFY2"
	},

	{
		"snmpReceiver" : "$SNMP_RECEIVER3",
		"snmpPort" : $SNMP_PORT3,
		"snmpVersion" : "$SNMP_VERSION3",
		"snmpCommunity" : "$SNMP_COMMUNITY3",
		"IS_SNMP_NOTIFY" : "$IS_SNMP_NOTIFY3"
	},

	{
		"snmpReceiver" : "$SNMP_RECEIVER4",
		"snmpPort" : $SNMP_PORT4,
		"snmpVersion" : "$SNMP_VERSION4",
		"snmpCommunity" : "$SNMP_COMMUNITY4",
		"IS_SNMP_NOTIFY" : "$IS_SNMP_NOTIFY4"
	}
]
EOF

	else

cat > $WORKINGDIR/$SNMP_FILE <<EOF
[
	{
		"snmpReceiver" : "$SNMP_RECEIVER1",
		"snmpPort" : $SNMP_PORT1,
		"snmpVersion" : "$SNMP_VERSION1",
		"snmpEngineId" : "$SNMP_ENGINE_ID1",
		"snmpAuthUser": "$SNMP_AUTH_USER1",
		"snmpAuthPassword": "$SNMP_AUTH_PASS1",
		"snmpAuthProtocol": "$SNMP_AUTH_PROTOCOL1",
		"snmpPrivProtocol": "$SNMP_PRIV_PROTOCOL1",
		"snmpPrivPassword": "$SNMP_PRIV_PASS1",
		"snmpCommunity" : "$SNMP_COMMUNITY1",
		"IS_SNMP_NOTIFY" : "$IS_SNMP_NOTIFY1"
	},

	{
		"snmpReceiver" : "$SNMP_RECEIVER2",
		"snmpPort" : $SNMP_PORT2,
		"snmpVersion" : "$SNMP_VERSION2",
		"snmpEngineId" : "$SNMP_ENGINE_ID2",
		"snmpAuthUser": "$SNMP_AUTH_USER2",
		"snmpAuthPassword": "$SNMP_AUTH_PASS2",
		"snmpAuthProtocol": "$SNMP_AUTH_PROTOCOL2",
		"snmpPrivProtocol": "$SNMP_PRIV_PROTOCOL2",
		"snmpPrivPassword": "$SNMP_PRIV_PASS2",
		"snmpCommunity" : "$SNMP_COMMUNITY2",
		"IS_SNMP_NOTIFY" : "$IS_SNMP_NOTIFY2"
	},

	{
		"snmpReceiver" : "$SNMP_RECEIVER3",
		"snmpPort" : $SNMP_PORT3,
		"snmpVersion" : "$SNMP_VERSION3",
		"snmpEngineId" : "$SNMP_ENGINE_ID3",
		"snmpAuthUser": "$SNMP_AUTH_USER3",
		"snmpAuthPassword": "$SNMP_AUTH_PASS3",
		"snmpAuthProtocol": "$SNMP_AUTH_PROTOCOL3",
		"snmpPrivProtocol": "$SNMP_PRIV_PROTOCOL3",
		"snmpPrivPassword": "$SNMP_PRIV_PASS3",
		"snmpCommunity" : "$SNMP_COMMUNITY3",
		"IS_SNMP_NOTIFY" : "$IS_SNMP_NOTIFY3"
	},

	{
		"snmpReceiver" : "$SNMP_RECEIVER4",
		"snmpPort" : $SNMP_PORT4,
		"snmpVersion" : "$SNMP_VERSION4",
		"snmpEngineId" : "$SNMP_ENGINE_ID4",
		"snmpAuthUser": "$SNMP_AUTH_USER4",
		"snmpAuthPassword": "$SNMP_AUTH_PASS4",
		"snmpAuthProtocol": "$SNMP_AUTH_PROTOCOL4",
		"snmpPrivProtocol": "$SNMP_PRIV_PROTOCOL4",
		"snmpPrivPassword": "$SNMP_PRIV_PASS4",
		"snmpCommunity" : "$SNMP_COMMUNITY4",
		"IS_SNMP_NOTIFY" : "$IS_SNMP_NOTIFY4"
	}
]
EOF
	fi

	cp $WORKINGDIR/$SNMP_FILE /opt/fms/solution/config/alarms

	docker secret create $SNMP_SECRET /opt/fms/solution/config/alarms/$SNMP_FILE
	sed -i 's/ALARMING_CONFIG_SNMP_FILE_SECRET=/ALARMING_CONFIG_SNMP_FILE_SECRET='${SNMP_SECRET}'/g' /opt/fms/solution/deployment/.env
fi
# Restart service

#docker service update --force fms_alarming
cd /opt/fms/solution/deployment/
./swarm.sh
