#!/bin/bash

service docker stop
apt-get -y purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
apt autoremove -y
umount /var/lib/docker
rm -rf /var/lib/docker
rm -rf /var/lib/containerd
rm -rf /etc/docker
read -p  "Fstab will open. Please remove lines related to /var/lib/docker. Press enter to continue"
vi /etc/fstab
rm $WORKINGDIR/.docker
vgremove docker_vg --force
echo "Docker removed. Server will reboot"
sleep 5
init 6