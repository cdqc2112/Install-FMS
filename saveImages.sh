#!/bin/bash
if [ "$UID" != 0 ]; then
    echo "Run as root"
    exit 1
fi

WORKINGDIR=${PWD}
CURVER=$(awk -F'=' '/- VERSION=/ {print$2}' /opt/fms/solution/deployment/docker-compose.yml)

mkdir $WORKINGDIR/images
cd /opt/fms/solution/deployment
./swarm.sh image-save -d $WORKINGDIR/images
cd $WORKINGDIR
echo
echo "Saving images_$CURVER.tgz to $WORKINGDIR"
echo
tar -czvf images_$CURVER.tgz images
rm -rf images
echo
echo "Done"