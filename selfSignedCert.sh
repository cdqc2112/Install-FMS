#! /bin/bash

# selSignedCert - 1.2
source $WORKINGDIR/setup.cfg
echo
echo "The rootCA.cert will be created in /opt/fms/solution/cer. It has to be installed in browser"
echo "or imported into Trusted Root Certification Authorities." 
echo "It should also be installed on the RTUs"
read -n 1 -r -s -p $'Press enter to continue...\n'
echo
read -e -p 'Enter the country name (2 letter code): ' -i "US" COUNTRY
read -e -p 'Enter the state or province: ' -i "State" STATE
read -e -p 'Enter the locality: ' -i "City" CITY

# Create root CA & Private key
set -euxo pipefail
openssl req -x509 \
            -sha256 -days 3650 \
            -nodes \
            -newkey rsa:2048 \
            -subj "/CN=${DOMAIN}/C=${COUNTRY}/L=${CITY}" \
            -keyout rootCA.key -out rootCA.cert 

# Generate Private key 

openssl genrsa -out ${DOMAIN}.key 2048

# Create csr conf

cat > csr.conf <<EOF
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = ${COUNTRY}
ST = ${STATE}
L = ${CITY}
CN = ${DOMAIN}

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = ${DOMAIN}
DNS.2 = *.${DOMAIN}

EOF

# create CSR request using private key

openssl req -new -key ${DOMAIN}.key -out ${DOMAIN}.csr -config csr.conf

# Create a external config file for the certificate

cat > cert.conf <<EOF

authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = ${DOMAIN}
DNS.2 = *.${DOMAIN}

EOF

# Create SSl with self signed CA

openssl x509 -req \
    -in ${DOMAIN}.csr \
    -CA rootCA.cert -CAkey rootCA.key \
    -CAcreateserial -out ${DOMAIN}.cert \
    -days 3650 \
    -sha256 -extfile cert.conf

mv ${DOMAIN}.cert ${DOMAIN}-certonly.cert
cat ${DOMAIN}-certonly.cert rootCA.cert > ${DOMAIN}.cert

openssl x509 -text -noout -in ${DOMAIN}.cert

echo "$(date): Certificate ${DOMAIN}.cert created" >> $LOGFILE
touch $WORKINGDIR/._certs