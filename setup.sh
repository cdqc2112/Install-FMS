#!/bin/bash
#set -euxo pipefail
WORKINGDIR=${PWD}
BG_BLUE="$(tput setab 4)"
BG_BLACK="$(tput setab 0)"
FG_GREEN="$(tput setaf 2)"
FG_WHITE="$(tput setaf 7)"
CURVER=$(awk -F'=' '/- VERSION=/ {print$2}' $WORKINGDIR/deployment/docker-compose.yml)

export WORKINGDIR

# Save screen
tput smcup
REPLY=
# Display menu until selection == 0
while [[ $REPLY != 0 ]]; do
  echo -n ${BG_BLUE}${FG_WHITE}
  clear
  cat <<- _EOF_
    
    
    Script to install Docker and setup FMS $CURVER on Ubuntu or CentOS

    Docker engine data

    Docker will require disk space on every nodes for the purpose of storing docker images, container logs and runtime temporary files.

    It is advised to allocate a specific partition for the /var/lib/docker directory, with at least 60Gb.
    If no specific partition is used, this 60Gb will be consummed on the root partition, so the root partition
    must be sized accordingly (increased by 60Gb from normal OS requirement).

    The usage intensity on this storage will not be related to the load applied to the server. A SSD class storage is recommanded.

    FMS data

    A dedicated disk volume is required for the FMS data, on the following two server nodes:

        The only node in single-server architecture, when not using NFS
        Replica nodes in Replication or Cross-site redundancy architectures

    The disk must be visible at the OS level as a block device (ex: /dev/sdb)

    The required capacity and bandwidth for the disk depends on the FMS usage, please refer to the sizing guide for actual values.
    
    50% of the total capacity will be used to store backups, 25% for FMS solution and data and
    25% will be reserved for LVM snapshot that will be used during data backup process.

    Refer to https://www.system-rescue.org/lvm-guide-en/Making-consistent-backups-with-LVM/ for more information.
    
    The disk must not initially be partitionned, as the installation process includes a specific partitioning process using LVM.

    Please Select:

    1.  Install FMS on Master node
    2.  Setup Worker/Replica node
    3.  Start FMS
    4.  Check services
    5.  Test ports
    6.  Test NFS share
    7.  Set or edit SNMP and email notifications
    8.  Update FMS
    9.  Save FMS images
    10. Remove FMS solution and data
    11. Remove Docker
    Q.  Quit

_EOF_

read -p "Enter selection [1-11] or Q to quit > " selection
      
# Clear area beneath menu
tput cup 10 0
echo -n ${BG_BLACK}${FG_GREEN}
tput ed
tput cup 11 0

# Act on selection
case $selection in
  1)  ./InstallMaster.sh
      ;;
  2)  ./InstallWorker.sh
      ;;
  3)  ./start-fms.sh
      ;;
  4)  ./watch_service.sh
      ;;
  5)  ./testPorts.sh
      ;;
  6)  ./nfsCheck.sh
      ;;
  7)  ./notification.sh
      ;;
  8)  ./update.sh
      ;;
  9)  ./saveImages.sh
      ;;
  10) ./clean.sh
      ;;
  11) ./rm_docker.sh
      ;;
  q)  clear
      break
      ;;
  Q)  clear
      break
      ;;
  *)  echo "Invalid entry."
      ;;
esac
printf "\n\nPress any key to continue."
read -n 1
done

# Restore screen
tput rmcup
if [ -f "info.txt" ];then
    rm info.txt
fi
touch info.txt
echo "OS Release" >> info.txt
cat /etc/os-release >> info.txt
echo "Docker" >> info.txt
docker info >> info.txt
echo "Memory" >> info.txt
free -h >> info.txt
clear
exit