#!/bin/bash
if [ "$UID" != 0 ]; then
    echo "Run as root"
    exit 1
fi

WORKINGDIR=${PWD}
LOGFILE=$WORKINGDIR/install.log
CURVER=$(awk -F'=' '/- VERSION=/ {print$2}' /opt/fms/solution/deployment/docker-compose.yml)
NEWVER=$(awk -F'=' '/- VERSION=/ {print$2}' $WORKINGDIR/deployment/docker-compose.yml)

if [ "$NEWVER" = "$CURVER" ];then
    echo "Current version $CURVER is up to date"
    exit 1
else
    echo "Current version $CURVER will be updated to $NEWVER"
fi
read -r -p 'Are you ready to update the FMS? [y/N] ' response
    if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then
        #Remove stack
        docker stack rm fms
        #Check process are stopped
        docker ps
        sleep 20
        #System prune
        docker system prune -af
        #Copy new files to /opt/fms/solution/deployment
        mv /opt/fms/solution/deployment /opt/fms/solution/deployment_$CURVER
        cp -r $WORKINGDIR/deployment /opt/fms/solution
        cp /opt/fms/solution/deployment_$CURVER/.env /opt/fms/solution/deployment
        cp /opt/fms/solution/deployment_$CURVER/secrets /opt/fms/solution
        cp /opt/fms/solution/deployment_$CURVER/docker-compose-local.yml /opt/fms/solution/deployment
        cd /opt/fms/solution/deployment
    read -r -p 'Do you want to remove /opt/fms/solution/data/dwh/ (For all Demo/POC version installed based on FMS 7.14.1 or 7.15.1)? [y/N] ' response
        if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]];then
            rm -rf /opt/fms/solution/data/dwh/
        fi 
    read -r -pread -n 1 -s -r -p 'Execute "docker system prune -af" on all managers and replica. Press any key when it is done '
        chmod +x swarm.sh
        ./swarm.sh --fill-secrets >> secrets_$NEWVER
    else
        exit 1
    fi